# Tricoteuses-UI

## _Live informations from french Parliament (Assemblée nationale & Sénat)_

![Screenshot of a Tricoteuses-UI](https://forum.parlement-ouvert.fr/uploads/default/original/1X/05bac0475f7e5946186a04713325a4bd35a5e125.png)

_Tricoteuses-UI_ is free and open source software.

* [software repository](https://framagit.org/tricoteuses/tricoteuses-ui)
* [GNU Affero General Public License version 3 or greater](https://framagit.org/tricoteuses/tricoteuses-ui/blob/master/LICENSE.md)

## Installation

### Install dependencies

```bash
npm install
```

### Create the database

```bash
su - postgres
createuser -D -P -R -S tricoteuses
  Enter password for new role: tricoteuses
  Enter it again: tricoteuses
createdb -E utf-8 -O tricoteuses tricoteuses
```

### Configure the web server

```bash
npm run configure
```

### Launch the web server

```bash
npm run start
```
