# Exemples de dossiers parlementaires de l'Assemblée nationale pour tests

## Tests des différents types d'actes

### AdoptionEurope

Aucun acte n'existe dans la XVème législature.

```bash
rg Adoption_Europe_Type
  Dossiers_Legislatifs_XIV.json
  1608881:                        "@xsi:type": "Adoption_Europe_Type",
  1608904:                        "@xsi:type": "Adoption_Europe_Type",
  1608927:                        "@xsi:type": "Adoption_Europe_Type",
  1608950:                        "@xsi:type": "Adoption_Europe_Type",
  1608973:                        "@xsi:type": "Adoption_Europe_Type",
  1608996:                        "@xsi:type": "Adoption_Europe_Type",
  1609019:                        "@xsi:type": "Adoption_Europe_Type
```

### ConclusionEtapeCc et SaisineConseilConstit

#### Dossier avec saisine du Conseil constitutionnel par plus de 60 députés et plus de 60 sénateurs, avec décision de conformité

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36388

#### Dossier avec saisine du Conseil constitutionnel par plus de 60 députés et plus de 60 sénateurs, avec décision de conformité partielle

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36410

#### Dossier avec saisine du Conseil constitutionnel  de droit, en application de l'article 61§2 de la Constitution

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36468

### CreationOrganeTemporaire

2 cas différents trouvés dans la XVème législature :
* Création d'une commission d'enquête
* Création d'une mission d'information

#### Dossier avec création d'une commission d'enquête

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36457

#### Dossier avec création d'une mission d'information

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36501

#### Dossier avec création d'une mission d'information dont l'organe a un uid inconnu

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36987

### Decision

Cet acte est présent dans un grand nombre de dossiers législatifs.

### DecisionMotionCensure et DepotMotionCensure

#### Dossier avec motion de censure rejetée

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N37071

### DecisionRecevabiliteBureau

Aucun acte n'existe dans la XVème législature et un seul dans la XIVème législature :
* DLR5L14N35369 Proposition de résolution visant à réunir la Haute Cour

### DeclarationGouvernement

Aucun acte n'existe dans la XVème législature et la XIVème législature contient 12 actes.

* DLR5L14N34049 Engagement de la responsabilité du Gouvernement : croissance, activité et égalité des chances économiques (nouvelle lecture)
  Procédure parlementaire : Engagement de la responsabilité gouvernementale
* DLR5L14N34783 Travail : modernisation du droit du travail
  Procédure parlementaire : Projet de loi ordinaire, avec une déclaration engageant la responsabilité du Gouvernement devant l'Assemblée nationale sur le vote d'un texte

Toutes les DeclarationGouvernement ont le typeDeclaration "Déclaration engageant la responsabilité du Gouvernement devant l'Assemblée nationale sur le vote d'un texte".

### DepotAccordInternational

* https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36076

### DepotAvisConseilEtat

Cet acte est présent dans un grand nombre de dossiers législatifs.

### DepotInitiative

Cet acte est présent dans un grand nombre de dossiers législatifs.

### DepotInitiativeNavette

Cet acte est présent dans un grand nombre de dossiers législatifs.

### DepotLettreRectificative

Aucun acte n'existe dans la XVème législature et la XIVème législature contient 3 actes.

Les 3 actes apparaissent dans des projets de loi ordinaire.

* DLR5L14N33488 Travail : désignation des prud'hommes

### DepotMotionReferendaire

Aucun acte n'existe dans la XVème législature et la XIVème législature contient 2 actes.

Les 2 actes apparaissent lors de discussions en séance publique au Sénat.

* DLR5L14N29305 Société : ouverture du mariage aux couples de même sexe
* DLR5L14N33251 Collectivités territoriales : délimitation des régions et calendrier électoral

### DepotRapport

Cet acte est présent dans un grand nombre de dossiers législatifs.

### DiscussionCommission

Cet acte est présent dans un grand nombre de dossiers législatifs.

### DiscussionSeancePublique

Cet acte est présent dans un grand nombre de dossiers législatifs.

### Etape

Cet acte est présent dans un grand nombre de dossiers législatifs.

### EtudeImpact

Cet acte est présent dans un grand nombre de dossiers législatifs.

### MotionProcedure

2 types de motions :
* Motion de renvoi en commission
* Question préalable

Les motions de renvoi en commission qui sont dans la base sont toutes adoptées.
Les questions préalables qui sont dans la base sont toutes rejetées.

#### Dossier avec motion de renvoi en commission adoptée

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36538

#### Dossier avec question préalable rejetée

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36612

### NominRapporteurs

#### Dossier avec nomination de rapporteurs à une date nulle

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36914

### ProcedureAcceleree

Cet acte est présent dans un grand nombre de dossiers législatifs.

### Promulgation

#### Dossier avec promulgation, parution au Journal officiel et parution d'un rectificatif

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36410

### RenvoiCmp

#### Dossier avec renvoi en commission mixte paritaire aboutissant sur un accord

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36910

#### Dossier avec renvoi en commission mixte paritaire aboutissant sur un désaccord

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36220

### RenvoiPrealable

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N36343

### RetraitInitiative

https://tricoteuses.fr/dossiers/assemblee/DLR5L15N35887

### SaisieComAvis

Cet acte est présent dans un grand nombre de dossiers législatifs.

### SaisieComFond

Cet acte est présent dans un grand nombre de dossiers législatifs.
