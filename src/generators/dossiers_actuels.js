import { newViewAgent } from "../model/dossiers_actuels"

export async function generateDossiersActuels(fs, path) {
  const agent = newViewAgent()
  console.log(`Generating ${agent.staticViewDataPath}...`)
  await agent.fetchGraphqlData()
  agent.writeStaticGraphqlData(fs, path)

  // agent.readStaticGraphqlData(fs, path)
  const improvedData = agent.improveGraphqlData()

  agent.generateViewData(improvedData)
  agent.writeStaticViewData(fs, path)
  return agent.viewData
}
