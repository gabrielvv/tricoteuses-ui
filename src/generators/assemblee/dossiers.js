import gql from "graphql-tag"

import { assembleeGraphqlClient } from "../../graphql/clients"
import { generateDossierAssemblee } from "./dossier"

export async function generateDossiersAssemblee(fs, path, uidsToSkip) {
  const graphqlData = await assembleeGraphqlClient.query({
    query: gql`
      query UidsDossiersAssemblee($legislature: Int!) {
        dossiersParlementaires(legislature: $legislature) {
          ... on DossierCommissionEnquete {
            uid
          }
          ... on DossierInitiativeExecutif {
            uid
          }
          ... on DossierLegislatif {
            uid
          }
          ... on DossierMissionControle {
            uid
          }
          ... on DossierMissionInformation {
            uid
          }
          ... on DossierResolutionAN {
            uid
          }
          ... on DossierSansType {
            uid
          }
        }
      }
    `,
    variables: {
      legislature: 15,
    },
  })
  const uids = graphqlData
    .data
    .dossiersParlementaires
    .map(dossier => dossier.uid)
    .filter(uid => !uidsToSkip || !uidsToSkip.has(uid))
    .sort()
  for (let uid of uids) {
    await generateDossierAssemblee(fs, path, uid)
  }
  return uids
}
