import { newViewAgent } from "../../model/assemblee/dossier"

export async function generateDossierAssemblee(fs, path, uid) {
  const agent = newViewAgent(uid)
  console.log(`Generating ${agent.staticViewDataPath}...`)
  await agent.fetchGraphqlData()
  agent.writeStaticGraphqlData(fs, path)

  // agent.readStaticGraphqlData(fs, path)
  const improvedData = agent.improveGraphqlData()

  agent.generateViewData(improvedData)
  agent.writeStaticViewData(fs, path)
  return agent.viewData
}
