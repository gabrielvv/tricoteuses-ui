import assert from "assert"
import gql from "graphql-tag"

import config from "../config"

export class GraphqlAgent {
  constructor(title, apiUrl, client, queryText, variables, staticGraphqlDataPath, improver) {
    this.apiUrl = apiUrl
    this.client = client
    this.graphqlData = null
    this.improver = improver || null
    this.query = gql(queryText)
    this.queryText = queryText
    this.staticGraphqlDataPath = staticGraphqlDataPath || null
    this.title = title
    this.useGraphqlCache = staticGraphqlDataPath && config.cache !== null
    this.variables = variables
  }

  async fetchGraphqlData() {
    const graphqlData = await this.client.query({ query: this.query, variables: this.variables })
    this.graphqlData = graphqlData
    return graphqlData
  }

  async fetchStaticGraphqlData(fetch) {
    const response = await fetch(`${this.staticGraphqlDataPath}`)
    assert(response.ok)
    const graphqlData = await response.json()
    this.graphqlData = graphqlData
    return graphqlData
  }

  improveGraphqlData() {
    return this.improver ? this.improver(this.graphqlData) : this.graphqlData
  }

  async retrieveData(fetch) {
    assert.notStrictEqual(fetch, undefined)
    if (this.useGraphqlCache) {
      await this.fetchStaticGraphqlData(fetch)
    } else {
      await this.fetchGraphqlData()
    }
    return this.improveGraphqlData()
  }

  writeStaticGraphqlData(fs, path) {
    assert.notStrictEqual(this.graphqlData, null)
    const staticGraphqlFilePath = path.join("static", this.staticGraphqlDataPath)
    fs.ensureDirSync(path.dirname(staticGraphqlFilePath))
    fs.writeFileSync(staticGraphqlFilePath, JSON.stringify(this.graphqlData, null, 2))
  }
}

export class GraphqlMultiAgent {
  constructor(agentBySymbol) {
    this.agentBySymbol = agentBySymbol
    this.useGraphqlCache = Object.values(agentBySymbol).some(agent => agent.useGraphqlCache)
  }

  async fetchGraphqlData() {
    await Promise.all(
      Object.values(this.agentBySymbol)
        .map(agent => agent.fetchGraphqlData())
    )
    return Object.entries(this.agentBySymbol)
      .reduce(
        (accumulator, [symbol, agent]) => {
          accumulator[symbol] = agent.graphqlData
          return accumulator
        },
        {},
      )
  }

  async fetchStaticGraphqlData(fetch) {
    await Promise.all(
      Object.values(this.agentBySymbol)
        .map(agent => agent.fetchStaticGraphqlData(fetch))
    )
    return Object.entries(this.agentBySymbol)
      .reduce(
        (accumulator, [symbol, agent]) => {
          accumulator[symbol] = agent.graphqlData
          return accumulator
        },
        {},
      )
  }

  improveGraphqlData() {
    return Object.entries(this.agentBySymbol)
      .reduce(
        (accumulator, [symbol, agent]) => {
          accumulator[symbol] = agent.improveGraphqlData()
          return accumulator
        },
        {},
      )
  }

  async retrieveData(fetch) {
    assert.notStrictEqual(fetch, undefined)
    const retrievedData = await Promise.all(
      Object.values(this.agentBySymbol)
        .map(agent => agent.retrieveData(fetch))
    )
    return [...Object.keys(this.agentBySymbol).entries()]
      .reduce(
        (accumulator, [index, symbol]) => {
          accumulator[symbol] = retrievedData[index]
          return accumulator
        },
        {},
      )
  }

  writeStaticGraphqlData(fs, path) {
    for (let agent of Object.values(this.agentBySymbol)) {
      agent.writeStaticGraphqlData(fs, path)
    }
  }
}

export class ViewAgent {
  constructor(graphqlAgent, staticViewDataPath, generator) {
    this.generator = generator
    this.graphqlAgent = graphqlAgent
    this.staticViewDataPath = staticViewDataPath
    this.useGraphqlCache = graphqlAgent.useGraphqlCache
    this.useViewCache = staticViewDataPath && config.cache === "view"
    this.viewData  = null
  }

  get agentBySymbol() {
    return this.graphqlAgent.agentBySymbol
  }

  async fetchGraphqlData() {
    return await this.graphqlAgent.fetchGraphqlData()
  }

  async fetchStaticGraphqlData(fetch) {
    return await this.graphqlAgent.fetchStaticGraphqlData(fetch)
  }

  async fetchStaticViewData(fetch) {
    const response = await fetch(`${this.staticViewDataPath}`)
    assert(response.ok)
    const viewData = await response.json()
    this.viewData = viewData
    return viewData
  }

  generateViewData(improvedData) {
    const viewData = this.generator ? this.generator(improvedData) : improvedData
    this.viewData = viewData
    return viewData
  }

  improveGraphqlData() {
    return this.graphqlAgent.improveGraphqlData()
  }

  async retrieveData(fetch) {
    assert.notStrictEqual(fetch, undefined)
    if (this.useViewCache) {
      return await this.fetchStaticViewData(fetch)
    } else {
      const improvedData = await this.graphqlAgent.retrieveData(fetch)
      return this.generateViewData(improvedData)
    }
  }

  writeStaticGraphqlData(fs, path) {
    this.graphqlAgent.writeStaticGraphqlData(fs, path)
  }

  writeStaticViewData(fs, path) {
    assert.notStrictEqual(this.viewData, null)
    const staticViewFilePath = path.join("static", this.staticViewDataPath)
    fs.ensureDirSync(path.dirname(staticViewFilePath))
    fs.writeFileSync(staticViewFilePath, JSON.stringify(this.viewData, null, 2))
  }
}