export function colorFromEtaloilib(etaloilib) {
  return etaloilib === "en cours de discussion"
    ? "grey-dark"
    : etaloilib === "fusionné"
      ? "blue"
      : etaloilib === "rejeté"
        ? "red"
        : etaloilib === "promulgué ou adopté (ppr)"
          ? "green"
          : etaloilib === "caduc"
            ? "blue"
            : "yellow"
}
