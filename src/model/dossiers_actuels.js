import assert from "assert"

import config from "../config"
import { generateDossierParlementaireFragments } from "../graphql/assemblee"
import { assembleeGraphqlClient, senatGraphqlClient, textFromFragments } from "../graphql/clients"
import { generateLoiFragments } from "../graphql/senat"
import {
  nextActePath,
  nextDate,
  previousActePath,
  previousDate,
  statusFromDossier,
  summaryStepFromActePath,
  summaryStepsFromDossier,
  unflattenDossierParlementaire,
} from "./assemblee/helpers"
import { GraphqlAgent, GraphqlMultiAgent, ViewAgent } from "./data_agents"

const dossiersAssembleeAVenirQueryText = `
  query dossiersAssembleeAVenir($date: String!) {
    dossiersParlementairesFuturs(date: $date) {
      ...DossierParlementaireFragment
    }
  }
  ${textFromFragments(generateDossierParlementaireFragments({ withDocuments: false }))}
`
  .trim()
  .replace(/^  /gm, "")

const dossiersAssembleeRecentsQueryText = `
  query dossiersAssembleeRecents($date: String!) {
    dossiersParlementairesRecents(date: $date) {
      ...DossierParlementaireFragment
    }
  }
  ${textFromFragments(generateDossierParlementaireFragments({ withDocuments: false }))}
`
  .trim()
  .replace(/^  /gm, "")

const dossiersSenatAVenirQueryText = `
  query DossiersLegislatifsFuturs($date: NaiveDate!) {
    dossiersLegislatifsFuturs(date: $date) {
      ...LoiFragment
    }
  }
  ${textFromFragments(generateLoiFragments())}
`
  .trim()
  .replace(/^  /gm, "")

const dossiersSenatRecentsQueryText = `
  query DossiersLegislatifsRecents($date: NaiveDate!) {
    dossiersLegislatifsRecents(date: $date) {
      ...LoiFragment
    }
  }
  ${textFromFragments(generateLoiFragments())}
`
  .trim()
  .replace(/^  /gm, "")

const senatCheminRegExp = /^https?:\/\/www\.senat\.fr\/dossier-legislatif\/(.+)\.html$/
const urlAnRegExps = [
  /^https?:\/\/www\.assemblee-nationale\.fr\/([0-9]+)\/dossiers\/(.+)\.asp$/,
  /^https?:\/\/www\.assemblee-nationale\.fr\/dyn\/([0-9]+)\/dossiers\/(.+)$/,
]

export function newGraphqlAgent() {
  const nowText = new Date().toISOString()
  return new GraphqlMultiAgent({
    dossiersAssembleeAVenir: new GraphqlAgent(
      "Dossiers législatifs à venir de l'Assemblée",
      config.assembleeApiUrl,
      assembleeGraphqlClient,
      dossiersAssembleeAVenirQueryText,
      {
        date: nowText,
      },
      "data/graphql/assemblee/dossiers_a_venir.json",
      function (graphqlData) {
        return graphqlData
          .data
          .dossiersParlementairesFuturs
          .map(unflattenDossierParlementaire)
          .map(dossier => {
            dossier.nextDateKnitted = nextDate(dossier, nowText)
            return dossier
          })
          .sort((a, b) => (a.nextDateKnitted || "").localeCompare(b.nextDateKnitted || ""))
      },
    ),
    dossiersAssembleeRecents: new GraphqlAgent(
      "Dossiers législatifs récents de l'Assemblée",
      config.assembleeApiUrl,
      assembleeGraphqlClient,
      dossiersAssembleeRecentsQueryText,
      {
        date: nowText,
      },
      "data/graphql/assemblee/dossiers_recents.json",
      function (graphqlData) {
        return graphqlData
          .data
          .dossiersParlementairesRecents
          .map(unflattenDossierParlementaire)
          .map(dossier => {
            dossier.previousDateKnitted = previousDate(dossier, nowText)
            return dossier
          })
          .sort((a, b) => (a.previousDateKnitted || "").localeCompare(b.previousDateKnitted || ""))
      },
    ),
    dossiersSenatAVenir: new GraphqlAgent(
      "Dossiers législatifs à venir du Sénat",
      config.senatApiUrl,
      senatGraphqlClient,
      dossiersSenatAVenirQueryText,
      {
        date: nowText.split("T")[0],
      },
      "data/graphql/senat/dossiers_a_venir.json",
      function (graphqlData) {
        return graphqlData
          .data
          .dossiersLegislatifsFuturs
      },
    ),
    dossiersSenatRecents: new GraphqlAgent(
      "Dossiers législatifs récents du Sénat",
      config.senatApiUrl,
      senatGraphqlClient,
      dossiersSenatRecentsQueryText,
      {
        date: nowText.split("T")[0],
      },
      "data/graphql/senat/dossiers_recents.json",
      function (graphqlData) {
        return graphqlData
          .data
          .dossiersLegislatifsRecents
      },
    ),
  })
}

export function newViewAgent() {
  const nowText = new Date().toISOString()
  return new ViewAgent(
    newGraphqlAgent(),
    "data/views/dossiers_actuels.json",
    function (improvedData) {
      const viewData = { ...improvedData }

      const dossiersAssembleeUids = new Set(
        viewData.dossiersAssembleeAVenir
          .map(dossierAssemblee => dossierAssemblee.uid)
      )
      let dossiersAssembleeActuels = [
        ...viewData.dossiersAssembleeRecents.filter(dossierAssemblee =>
          !dossiersAssembleeUids.has(dossierAssemblee.uid)),
        ...viewData.dossiersAssembleeAVenir,
      ]
      delete viewData.dossiersAssembleeAVenir
      delete viewData.dossiersAssembleeRecents

      dossiersAssembleeActuels = dossiersAssembleeActuels
        .map(dossierAssemblee => {
          dossierAssemblee = { ...dossierAssemblee }
          dossierAssemblee.currentDateKnitted =
            dossierAssemblee.nextDateKnitted
            || dossierAssemblee.previousDateKnitted
            || null
          {
            const actePath = nextActePath(dossierAssemblee, nowText)
            if (actePath === null) {
              dossierAssemblee.nextSummaryStepKnitted = null
            } else {
              const leafActe = actePath[actePath.length - 1]
              dossierAssemblee.nextSummaryStepKnitted = {
                ...summaryStepFromActePath(dossierAssemblee, actePath),
                date: leafActe.dateActe,
                libelle: leafActe.libelleActe.libelleCourt || leafActe.libelleActe.nomCanonique,
              }
            }
          }
          {
            const actePath = previousActePath(dossierAssemblee, nowText)
            if (actePath === null) {
              dossierAssemblee.previousSummaryStepKnitted = null
            } else {
              const leafActe = actePath[actePath.length - 1]
              dossierAssemblee.previousSummaryStepKnitted = {
                ...summaryStepFromActePath(dossierAssemblee, actePath),
                date: leafActe.dateActe,
                libelle: leafActe.libelleActe.libelleCourt || leafActe.libelleActe.nomCanonique,
              }
            }
          }
          dossierAssemblee.statusKnitted = statusFromDossier(dossierAssemblee)
          dossierAssemblee.summaryStepsKnitted = summaryStepsFromDossier(dossierAssemblee)

          delete dossierAssemblee.actesLegislatifs
          return dossierAssemblee
        })

      const dossierAssembleeBySignetSenat = {}
      const dossierAssembleeByTitreCheminByLegislature = {}
      for (let dossierAssemblee of dossiersAssembleeActuels) {
        const titreDossier = dossierAssemblee.titreDossier
        if (titreDossier.senatChemin !== null) {
          const match = titreDossier.senatChemin.match(senatCheminRegExp)
          assert.notStrictEqual(match, null, `Invalid senatChemin: ${titreDossier.senatChemin}`)
          const signetSenat = match[1]
          dossierAssemblee.signetSenatKnitted = signetSenat
          dossierAssembleeBySignetSenat[signetSenat] = dossierAssemblee
        } else {
          dossierAssemblee.signetSenatKnitted = null
        }
        if (titreDossier.titreChemin !== null) {
          let dossierAssembleeByTitreChemin = dossierAssembleeByTitreCheminByLegislature[dossierAssemblee.legislature]
          if (dossierAssembleeByTitreChemin === undefined) {
            dossierAssembleeByTitreChemin =
              dossierAssembleeByTitreCheminByLegislature[dossierAssemblee.legislature] = {}
          }
          dossierAssembleeByTitreChemin[titreDossier.titreChemin] = dossierAssemblee
        }
      }

      const dossiersSenatLoicods = new Set(
        viewData.dossiersSenatAVenir
          .map(dossierSenat => dossierSenat.loicod)
      )
      const dossiersSenatActuels = [
        ...viewData.dossiersSenatRecents.filter(dossierSenat => !dossiersSenatLoicods.has(dossierSenat.loicod)),
        ...viewData.dossiersSenatAVenir,
      ]
      delete viewData.dossiersSenatAVenir
      delete viewData.dossiersSenatRecents

      const dossierSenatBySignet = {}
      const dossierSenatByTitreCheminByLegislatureAssemblee = {}
      for (let dossierSenat of dossiersSenatActuels) {
        if (dossierSenat.signet !== null) {
          dossierSenatBySignet[dossierSenat.signet] = dossierSenat
        }
        if (dossierSenat.urlAn !== null) {
          let match = null
          for (let urlAnRegExp of urlAnRegExps) {
            match = dossierSenat.urlAn.match(urlAnRegExp)
            if (match !== null) {
              break
            }
          }
          assert.notStrictEqual(match, null, `Invalid urlAn: ${dossierSenat.urlAn}`)
          const legislature = match[1]
          const titreChemin = match[2]
          let dossierSenatByTitreChemin = dossierSenatByTitreCheminByLegislatureAssemblee[legislature]
          if (dossierSenatByTitreChemin === undefined) {
            dossierSenatByTitreChemin = dossierSenatByTitreCheminByLegislatureAssemblee[legislature] = {}
          }
          dossierSenat.legislatureKnitted = legislature
          dossierSenat.titreCheminKnitted = titreChemin
          dossierSenatByTitreChemin[titreChemin] = dossierSenat
        } else {
          dossierSenat.legislatureKnitted = null
          dossierSenat.titreCheminKnitted = null
        }
      }

      const dossiersActuelsLoicodsSenat = new Set()
      const dossiersActuels = []
      for (let dossierAssemblee of dossiersAssembleeActuels) {
        let dossierSenat = null
        if (dossierAssemblee.signetSenatKnitted !== null) {
          const dossier = dossierSenatBySignet[dossierAssemblee.signetSenatKnitted]
          if (dossier !== undefined) {
            dossierSenat = dossier
            dossiersActuelsLoicodsSenat.add(dossierSenat.loicod)
          }
        }
        const titreDossier = dossierAssemblee.titreDossier
        if (titreDossier.titreChemin !== null) {
          const dossierSenatByTitreChemin = dossierSenatByTitreCheminByLegislatureAssemblee[
            dossierAssemblee.legislature]
          if (dossierSenatByTitreChemin !== undefined) {
            let dossier = dossierSenatByTitreChemin[titreDossier.titreChemin]
            if (dossier !== undefined) {
              if (dossierSenat === null) {
                dossierSenat = dossier
                dossiersActuelsLoicodsSenat.add(dossierSenat.loicod)
              } else {
                assert.strictEqual(dossierSenat.loicod, dossier.loicod)
              }
            }
          }
        }
        dossiersActuels.push({
          assemblee: dossierAssemblee,
          // TODO first: "assemblee" ou "senat",
          senat: dossierSenat,
        })
      }
      for (let dossierSenat of dossiersSenatActuels) {
        if (!dossiersActuelsLoicodsSenat.has(dossierSenat.loicod)) {
          dossiersActuels.push({
            assemblee: null,
            first: "senat",
            senat: dossierSenat,
          })
        }
      }
      viewData.dossiersActuelsKnitted = dossiersActuels

      return viewData
    },
  )
}
