import assert from "assert"

import { uncapitalizeFirstLetter } from "../../strings"

const documentUrlFragmentsByType = {
  ACIN: {
    directory: "projets",
    prefix: "pl",
    suffix: "-ai",
  },
  AVCE: {
    directory: "projets",
    prefix: "pl",
    suffix: "-ace",
  },
  AVIS: {
    directory: "rapports",
    prefix: "r",
    suffix: "",
  },
  ETDI: {
    directory: "projets",
    prefix: "pl",
    suffix: "-ei",
  },
  LETT: {
    directory: "projets",
    prefix: "pl",
    suffix: "-l",
  },
  PION: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNRE: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNREAPPART341: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNRECOMENQ: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNREMODREGLTAN: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNRETVXINSTITEUROP: {
    directory: "europe/resolutions",
    prefix: "ppe",
    suffix: "",
  },
  PRJL: {
    directory: "projets",
    prefix: "pl",
    suffix: "",
  },
  RAPP: {
    directory: "rapports",
    prefix: "r",
    suffix: "",
  },
  RINF: {
    directory: "rapports",
    prefix: "r",
    suffix: "",
  },
  RION: {
    directory: "",
    prefix: "",
    suffix: "",
  },
  TADO: {
    directory: "ta",
    prefix: "ta",
    suffix: "",
  },
  TCOM: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
  TCOMCOMENQ: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
  TCOMMODREGLTAN: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
  TCOMTVXINSTITEUROP: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
}

export function colorFromCodierLibelle(libelle) {
  switch (libelle) {
    case "Accord":
    case "adopté":
    case "adopté, dans les conditions prévues à l'article 45, alinéa 3, de la Constitution":
    case "adoptée":
    case "adopté sans modification":
    case "adoptée sans modification":
    case "Conforme":
    case "considérée comme définitive en application de l'article 151-7 du Règlement":
    case "Motion adopté(e)":
      return "green"
    case "considérée comme définitive en application de l'article 151-9 du Règlement":
      return "grey-dark"
    case "Désaccord":
    case "Motion rejeté(e)":
    case "rejeté":
    case "rejeté":
    case "rejetée":
      return "red"
    case "adopté avec modifications":
    case "adoptée avec modifications":
    case "De droit (article 61 alinéa 1 de la Constitution)":
    case "modifié":
    case "modifiée":
    case "Motion de censure 49-2":
    case "Partiellement conforme":
    case "Soixante députés au moins":
    case "Soixante sénateurs au moins":
      return "orange"
    default:
      return "yellow"
  }
}

export function colorFromOrganeCodeType(code) {
  if (code === null) {
    return null
  }
  return {
    ASSEMBLEE: "assemblee",
    CE: "conseil-etat",  // Conseil d'État
    CMP: "yellow-darker",  // Commission mixte paritaire
    CONSTITU: "conseil-constitutionnel",  // Conseil constitutionnel
    GOUVERNEMENT: "gouvernement",
    INTERNET: "teal",
    PRESREP: "presidence",  // Présidence de la République
    SENAT: "senat",
  }[code] || "yellow-darker"
}

export function nextActePath(dossier, now) {
  let nextDate = null
  let nextPath = null
  if (dossier.actesLegislatifs) {
    for (let acte of dossier.actesLegislatifs) {
      const [date, path] = nextDateAndPathForActe(acte, now)
      if (date && date >= now && (nextDate === null || date < nextDate)) {
        nextDate = date
        nextPath = path
      }
    }
  }
  return nextPath
}

export function nextDate(dossier, now) {
  let nextDate = null
  if (dossier.actesLegislatifs) {
    for (let acte of dossier.actesLegislatifs) {
      const date = nextDateAndPathForActe(acte, now)[0]
      if (date && date >= now && (nextDate === null || date < nextDate)) {
        nextDate = date
      }
    }
  }
  return nextDate
}

function nextDateAndPathForActe(acte, now) {
  let nextDate = acte.dateActe !== null && acte.dateActe >= now ? acte.dateActe : null
  let nextPath = []
  if (acte.actesLegislatifs) {
    for (let child of acte.actesLegislatifs) {
      const [date, path] = nextDateAndPathForActe(child, now)
      if (date && date >= now && (nextDate === null || date < nextDate)) {
        nextDate = date
        nextPath = path
      }
    }
  }
  if (nextDate === null) {
    nextPath = null
  } else {
    nextPath.unshift(acte)
  }
  return [nextDate, nextPath]
}

export function previousActePath(dossier, now) {
  let previousDate = null
  let previousPath = null
  if (dossier.actesLegislatifs) {
    for (let acte of [...dossier.actesLegislatifs].reverse()) {
      const [date, path] = previousDateAndPathForActe(acte, now)
      if (date && date < now && (previousDate === null || date > previousDate)) {
        previousDate = date
        previousPath = path
      }
    }
  }
  return previousPath
}

export function previousDate(dossier, now) {
  let previousDate = null
  if (dossier.actesLegislatifs) {
    for (let acte of [...dossier.actesLegislatifs].reverse()) {
      const date = previousDateAndPathForActe(acte, now)[0]
      if (date && date < now && (previousDate === null || date > previousDate)) {
        previousDate = date
      }
    }
  }
  return previousDate
}

function previousDateAndPathForActe(acte, now) {
  let previousDate = acte.dateActe !== null && acte.dateActe < now ? acte.dateActe : null
  let previousPath = []
  if (acte.actesLegislatifs) {
    for (let child of [...acte.actesLegislatifs].reverse()) {
      const [date, path] = previousDateAndPathForActe(child, now)
      if (date && date < now && (previousDate === null || date > previousDate)) {
        previousDate = date
        previousPath = path
      }
    }
  }
  if (previousDate === null) {
    previousPath = null
  } else {
    previousPath.unshift(acte)
  }
  return [previousDate, previousPath]
}

export function shortNameFromOrgane(organe) {
  if (organe === null) {
    return "Organe inconnu"
  }
  switch (organe.codeType) {
    case "CMP":
      return "Commission mixte paritaire"
    case "GOUVERNEMENT":
      return organe.libelle
    default:
      return organe.libelleAbrege
  }
}

export function statusFromActePath(dossier, actePath) {
  const leafActe = actePath[actePath.length - 1]
  let libelle = null
  switch (leafActe.__typename) {
    case "AdoptionEurope":
      libelle = leafActe.statutAdoption.libelle
      return {
        color: colorFromCodierLibelle(libelle),
        finalSuccess: null,
        label: uncapitalizeFirstLetter(libelle),
      }
    case "ConclusionEtapeCc":
      libelle = leafActe.statutConclusion.libelle
      return {
        color: colorFromCodierLibelle(libelle),
        finalSuccess: null,
        label: uncapitalizeFirstLetter(libelle),
      }
    case "Decision":
      libelle = leafActe.statutConclusion.libelle
      let color = colorFromCodierLibelle(libelle)
      let finalSuccess = null
      switch (dossier.procedureParlementaire.code) {
        case "PROCEDURE_PARLEMENTAIRE_2":
          // Proposition de loi ordinaire
          // Not sure for other cases => To improve.
          if (["rejeté", "rejetée"].includes(libelle) && dossier.actesLegislatifs.length === 1) {
            finalSuccess = false
          }
          break
        case "PROCEDURE_PARLEMENTAIRE_8":
          // Résolution
        case "PROCEDURE_PARLEMENTAIRE_22":
          // Résolution Article 34-1
          if (color === "green") {
            finalSuccess = true
          } else if (color === "red") {
            finalSuccess = false
          }
          break
        default:
          break
      }
      return {
        color,
        finalSuccess,
        label: uncapitalizeFirstLetter(libelle),
      }
    case "DecisionMotionCensure":
      libelle = leafActe.decision.libelle
      return {
        color: colorFromCodierLibelle(libelle),
        finalSuccess:
          // Not sure for other cases => To improve.
          // Engagement de la responsabilité gouvernementale
          dossier.procedureParlementaire.code === "PROCEDURE_PARLEMENTAIRE_13"
            && libelle === "Motion rejeté(e)"
          ? false
          : true,  // TODO: Not sure of this `true` value
        label: uncapitalizeFirstLetter(libelle),
      }
    case "DecisionRecevabiliteBureau":
      libelle = leafActe.decision.libelle
      return {
        color: colorFromCodierLibelle(libelle),
        finalSuccess: null,
        label: uncapitalizeFirstLetter(libelle),
      }
    case "DeclarationGouvernement":
      libelle = leafActe.typeDeclaration.libelle
      return {
        color: colorFromCodierLibelle(libelle),
        finalSuccess: null,
        label: uncapitalizeFirstLetter(libelle),
      }
    case "DepotMotionCensure":
      libelle = leafActe.typeMotionCensure.libelle
      return {
        color: colorFromCodierLibelle(libelle),
        finalSuccess: null,
        label: uncapitalizeFirstLetter(libelle),
      }
    case "DepotRapport":
      if (
        [
          "DossierCommissionEnquete",
          "DossierMissionControle",
          "DossierMissionInformation",
        ].includes(dossier.__typename)
      ) {
        return {
          color: colorFromOrganeCodeType(actePath[0].organe.codeType),
          finalSuccess: true,
          label: "rapport déposé",
        }
      }
      return null
    case "MotionProcedure":
      libelle = leafActe.typeMotion.libelle
      return {
        color: colorFromCodierLibelle(libelle),
        finalSuccess: null,
        label: uncapitalizeFirstLetter(libelle),
      }
    case "Promulgation":
      return {
        color: "green",
        finalSuccess: true,
        label: "promulgué",
      }
    case "RetraitInitiative":
      return {
        color: "red",
        finalSuccess: false,
        label: "retiré",
      }
    case "SaisineConseilConstit":
      libelle = leafActe.casSaisine.libelle
      return {
        color: colorFromCodierLibelle(libelle),
        finalSuccess: null,
        label: uncapitalizeFirstLetter(libelle),
      }
    default:
      // This acte gives no status.
      return null
  }
}

function statusFromActes(dossier, actePath, actes) {
  for (let acte of [...actes].reverse()) {
    const status = acte.actesLegislatifs
      ? statusFromActes(dossier, [...actePath, acte], acte.actesLegislatifs)
      : statusFromActePath(dossier, [...actePath, acte])
    if (status !== null) {
      return status
    }
    // No status found, continue with previous acte.
  }
  return null
}

export function statusFromDossier(dossier) {
  const status = statusFromActes(dossier, [], dossier.actesLegislatifs)
  if (status !== null) {
    return status
  }
  return {
    color: "grey-dark",
    label: "en cours",
  }
}

export function summaryStepFromActePath(dossier, actePath) {
  const rootActe = actePath[0]
  let organeCodeType = rootActe.organe.codeType
  let organeName = shortNameFromOrgane(rootActe.organe)
  let title = rootActe.libelleActe.libelleCourt || rootActe.libelleActe.nomCanonique
  if (rootActe.codeActe === "CMP") {
    const level1Acte = actePath[1]
    if (level1Acte !== undefined && level1Acte.codeActe.startsWith("CMP-DEBATS-")) {
      organeCodeType = level1Acte.organe.codeType
      organeName = shortNameFromOrgane(level1Acte.organe)
      title = "Lecture du texte de la Commission mixte paritaire"
    }
  }
  if (title !== null && title.toLowerCase() === organeName.toLowerCase()) {
    title = null
  }
  return {
    color: colorFromOrganeCodeType(organeCodeType),
    organeName,
    status: statusFromActePath(dossier, actePath),
    title,
  }
}

export function summaryStepsFromDossier(dossier) {
  const summarySteps = []
  for (let rootActe of dossier.actesLegislatifs) {
    summaryStepsFromRootActe(dossier, rootActe, summarySteps)
  }
  return summarySteps
}

export function summaryStepsFromRootActe(dossier, rootActe, summarySteps) {
  let organeCodeType = rootActe.organe.codeType
  let organeName = shortNameFromOrgane(rootActe.organe)
  let title = rootActe.libelleActe.libelleCourt || rootActe.libelleActe.nomCanonique
  if (title !== null && title.toLowerCase() === organeName.toLowerCase()) {
    title = null
  }
  summarySteps.push({
    color: colorFromOrganeCodeType(organeCodeType),
    organeName,
    status: statusFromActes(dossier, [rootActe], rootActe.actesLegislatifs),
    title,
  })
  if (rootActe.codeActe === "CMP") {
    for (let level1Acte of rootActe.actesLegislatifs || []) {
      if (level1Acte.codeActe.startsWith("CMP-DEBATS-")) {
        organeCodeType = level1Acte.organe.codeType
        organeName = shortNameFromOrgane(level1Acte.organe)
        title = "Lecture du texte de la Commission mixte paritaire"
        summarySteps.push({
          color: colorFromOrganeCodeType(organeCodeType),
          organeName,
          status: statusFromActes(dossier, [rootActe, level1Acte], level1Acte.actesLegislatifs),
          title,
        })
      }
    }
  }
}

export function textFromTypeRapporteur(typeRapporteur) {
  return {
    RAPPORTEUR: "rapporteur",
    RAPPORTEUR_GENERAL: "rapporteur général",
    RAPPORTEUR_POUR_AVIS: "rapporteur pour avis",
    RAPPORTEUR_SPECIAL: "rapporteur spécial",
  }[typeRapporteur] || typeRapporteur
}

function unflattenActeLegislatif(acte, allActes) {
  acte = { ...acte }
  if (acte.actesLegislatifsIndexes) {
    acte.actesLegislatifs = acte.actesLegislatifsIndexes
      .map(acteIndex => allActes[acteIndex])
      .map(acte => unflattenActeLegislatif(acte, allActes))
    delete acte.actesLegislatifsIndexes
  }
  if (acte.texteAdopte) {
    acte.texteAdopte = unflattenDocument(acte.texteAdopte)
  }
  if (acte.texteAssocie) {
    acte.texteAssocie = unflattenDocument(acte.texteAssocie)
  }
  return acte
}

export function unflattenDocument(documents, index = 0) {
  if (documents.length === 0) {
    return null
  }
  let document = documents[index]
  assert.strictEqual(document.documentsIndexes[0], index)
  document = {
    ...document,
    divisions: document.documentsIndexes.slice(1)
      .map(documentIndex => unflattenDocument(documents, documentIndex)),
  }
  delete document.documentsIndexes
  return document
}

export function unflattenDossierParlementaire(dossier) {
  const actesLegislatifs = dossier.actesLegislatifs
  const allActes = actesLegislatifs.actes
  return {
    ...dossier,
    actesLegislatifs: actesLegislatifs.actesIndexes
      .map(acteIndex => allActes[acteIndex])
      .map(acte => unflattenActeLegislatif(acte, allActes)),
  }
}

export function unflattenTexteLeg(texteLeg) {
  if (texteLeg === null) {
    return null
  }
  return {
    ...texteLeg,
    texteLegislatif: unflattenDocument(texteLeg.texteLegislatif),
  }
}

export function urlFromDocument(document, format) {
  // Code taken from function `an_text_url` in project anpy:
  // https://github.com/regardscitoyens/anpy/blob/master/anpy/dossier_from_opendata.py
  // See http://www.assemblee-nationale.fr/opendata/Implementation_Referentiel/Identifiants_Referentiels.html
  // for a description of the format of an UID.

  const extension = {
    "html": ".asp",
    "pdf": ".pdf",
    "raw-html": ".asp",  // The version of the document that can be parsed to extract divisions & articles
  }[format]
  assert.notStrictEqual(extension, undefined, `Unexpected format for urlFromDocument: ${format}`)
  const match = /^(.{4})([ANS]*)(R[0-9])([LS]*)([0-9]*)([BTACP]*)(.*)$/.exec(document.uid)
  if (match === null) {
    console.log(`Unable to generate URL from document with unexpected UID: "${document.uid}"`)
    return null
  }
  const legislature = match[5]
  let number = match[7]
  if (format === "raw-html") {
    return `http://www.assemblee-nationale.fr/${legislature}/textes/${number}${extension}`
  }
  const documentType = {
    BTC: "TCOM",
    BTA: "TADO",
  }[match[6]] || document.classification.typeClassification.code
  if (document.classification.sousType !== null && document.classification.sousType.code === "COMPA") {
    // Annexe au rapport : Texte comparatif COMPA
    number = number.replace(/-COMPA/, "-aCOMPA")
  }
  const fragments = documentUrlFragmentsByType[documentType]
  if (fragments === undefined) {
    // For example: ALCNANR5L15B0002 (allocution du président)
    console.log(`Unable to generate URL from document of UID "${document.uid}" and type "${documentType}"`)
    return null
  }
  const directory = format === "pdf" ? `pdf/${fragments.directory}` : fragments.directory
  return `http://www.assemblee-nationale.fr/${legislature}/${directory}/${
    fragments.prefix}${number}${fragments.suffix}${extension}`
}

export function urlFromScrutin(scrutin) {
  // Sample UIDs: VTANR5L15V389
  const match = /^VTANR([0-9]+)L([0-9]+)V([0-9]+)$/.exec(scrutin.uid)
  if (match === null) {
    console.log(`Unable to generate URL from scrutin with unexpected UID: "${scrutin.uid}"`)
    return null
  }
  const legislature = match[2]
  const numero = match[3]
  return `http://www2.assemblee-nationale.fr/scrutins/detail/(legislature)/${legislature}/(num)/${numero}`
}
