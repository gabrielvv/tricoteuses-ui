import assert from "assert"

import config from "../../config"
import { displayTimeFromString } from "../../dates"
import { generateDossierParlementaireFragments } from "../../graphql/assemblee"
import { assembleeGraphqlClient, textFromFragments } from "../../graphql/clients"
import {
  colorFromOrganeCodeType,
  shortNameFromOrgane,
  statusFromDossier,
  summaryStepsFromDossier,
  unflattenDossierParlementaire,
} from "./helpers"
import { GraphqlAgent, ViewAgent } from "../data_agents"

const dossierParlementaireQueryText = `
  query DossierParlementaire($date: String!, $uid: String!) {
    dossierParlementaire(uid: $uid) {
      ...DossierParlementaireFragmentWithDocuments
    }
  }
  ${textFromFragments(generateDossierParlementaireFragments({ withDocuments: true }))}
`
  .trim()
  .replace(/^  /gm, "")

export function newGraphqlAgent(uid) {
  const dateText = new Date().toISOString().split("T")[0]
  return new GraphqlAgent(
    "Dossier législatif de l'Assemblée",
    config.assembleeApiUrl,
    assembleeGraphqlClient,
    dossierParlementaireQueryText,
    {
      date: dateText,
      uid,
    },
    `data/graphql/assemblee/dossiers/${uid}.json`,
    function (graphqlData) {
      return unflattenDossierParlementaire(
        graphqlData
          .data
          .dossierParlementaire
      )
    },
  )
}

export function newViewAgent(uid) {
  return new ViewAgent(
    newGraphqlAgent(uid),
    `data/views/assemblee/dossiers/${uid}.json`,
    function (dossier) {
      dossier = { ...dossier }

      // Convert tree of actes to sequential steps.
      const steps = []
      const documentByUid = {}
      for (let acte of dossier.actesLegislatifs) {
        const actesSequence = []
        const context = {
          texteLoiUid: null,
        }
        stepsFromActe(actesSequence, acte, context, documentByUid, steps)
      }

      // Sort steps.
      // Before sort, add index to each step to be sure to have a stable sort by date.
      steps.map((step, index) => {
        step.index = index
        return step
      })
      steps.sort((step1, step2) => {
        if (step1.date === null || step2.date === null) {
          // When a step has no date, keep the original order.
          return step1.index - step2.index
        }
        // Ensure that dates without time are sorted after dates with time.
        const date1 = step1.date.replace(/T00:00:00/, "T99:99:99")
        const date2 = step2.date.replace(/T00:00:00/, "T99:99:99")
        return date1 < date2
          ? -1
          : date1 === date2
            ? step1.index - step2.index
            : 1
        })

      // Group steps by nodes, blocks & sub-blocks.
      let currentDate = null
      let node = null
      const nodes = []
      for (let step of steps) {
        const date = step.date === null ? null : step.date.split("T")[0].split("+")[0]
        if (date !== currentDate) {
          currentDate = date
          node = {
            blockByKey: {},
            date,
            keys: [],
          }
          nodes.push(node)
        }
        const blockKey = `${step.originOrganeName || "zz"}$${step.organeName}$${step.title}`
        let block = node.blockByKey[blockKey]
        if (block === undefined) {
          node.keys.push(blockKey)
          block = node.blockByKey[blockKey] = {
            blockByKey: {},
            color: step.color,
            id: step.leafActe.uid,
            keys: [],
            organeName: step.organeName,
            originColor: step.originColor,
            originOrganeName: step.originOrganeName,
            title: step.title,
          }
        }
        const leafOrganeShortName = shortNameFromOrgane(step.leafActe.organe)
        const subBlockKey =
          ["SaisieComAvis", "SaisieComFond"].includes(step.typeActe)
          || leafOrganeShortName === block.organeName
          || leafOrganeShortName === block.originOrganeName
          || leafOrganeShortName === shortNameFromOrgane(step.rootActe.organe)
            ? ""
            : leafOrganeShortName
        let subBlock = block.blockByKey[subBlockKey]
        if (subBlock === undefined) {
          if (subBlockKey) {
            block.keys.push(subBlockKey)
          } else {
            // The main sub-block must always be the first.
            block.keys.unshift(subBlockKey)
          }
          subBlock = block.blockByKey[subBlockKey] = {
            codeActe: step.leafActe.codeActe,
            id: step.leafActe.uid,
            steps: [],
            title: subBlockKey,
          }
        }
        subBlock.steps.push(step)
      }

      dossier.documentByUidKnitted = documentByUid
      dossier.nodesKnitted = nodes
      dossier.procedureAccelereeKnitted = procedureAccelereeFromActes(dossier.actesLegislatifs)
      dossier.statusKnitted = statusFromDossier(dossier)
      dossier.summaryStepsKnitted = summaryStepsFromDossier(dossier)

      return dossier
    },
  )
}

function procedureAccelereeFromActes(actes) {
  for (let acte of actes) {
    if (acte.actesLegislatifs) {
      let procedureAcceleree = procedureAccelereeFromActes(acte.actesLegislatifs)
      if (procedureAcceleree !== null) {
        return procedureAcceleree
      }
    } else if (acte.__typename === "ProcedureAcceleree") {
      return acte
    }
  }
  return null
}

function stepsFromActe(ancestors, acte, context, documentByUid, steps) {
  const actesLegislatifs = acte.actesLegislatifs
  acte = { ...acte }
  delete acte.actesLegislatifs
  ancestors.push(acte)
  if (actesLegislatifs) {
    // Occurs only when acte.__typename === "Etape"
    for (let child of actesLegislatifs) {
      stepsFromActe([...ancestors], child, context, documentByUid, steps)
    }
  } else {
    // Acte is a leaf acte.
    if (acte.dateActe) {
      const time = displayTimeFromString(acte.dateActe)
      if (time !== "0h00") {
        acte.time = time
      }
    }
    const rootActe = ancestors[0]
    let organeCodeType = rootActe.organe.codeType
    let organeName = shortNameFromOrgane(rootActe.organe)
    let originOrganeCodeType = null
    let originOrganeName = null
    let title = rootActe.libelleActe.libelleCourt || rootActe.libelleActe.nomCanonique
    let skipRootActe = true
    switch (acte.__typename) {
      case "DepotAvisConseilEtat":
        originOrganeCodeType = "CE"
        originOrganeName = "Conseil d'État"
        // title = null
        break
      case "DepotInitiative":
      case "EtudeImpact":
        const texteAssocie = acte.texteAssocie
        if (texteAssocie !== null) {
          if (
            texteAssocie.classification.famille !== null
            && texteAssocie.classification.famille.classe.code === "PRJLOI"
          ) {
            // A "projet de loi" always come from Gouvernement.
            originOrganeCodeType = "GOUVERNEMENT"
            originOrganeName = "Gouvernement"
          } else {
            originOrganeCodeType = "AUTRE"
            originOrganeName = texteAssocie.auteurs
              .filter(auteur => auteur.acteur !== null)
              .map(({ acteur }) => `${acteur.acteur.etatCivil.ident.prenom} ${acteur.acteur.etatCivil.ident.nom}`)
              .join(", ")
          }
        }
        break
      case "DepotInitiativeNavette":
        if (acte.provenance !== null) {
          originOrganeCodeType = acte.provenance.codeType
          originOrganeName = shortNameFromOrgane(acte.provenance)
        }
        break
      case "ProcedureAcceleree":
        organeCodeType = acte.organe.codeType
        organeName = shortNameFromOrgane(acte.organe)
        title = null
        break
      case "RenvoiCmp":
        const initiateur = acte.initiateur
        if (initiateur !== null) {
          let organe = null
          if (initiateur.acteurs.length > 0) {
            let mandat = initiateur.acteurs[0].mandat
            if (mandat !== null && mandat.organes.length > 0) {
              organe = mandat.organes[0]
            }
          } else if (initiateur.organes.length > 0) {
            organe = initiateur.organes[0]
          }
          if (organe !== null) {
            while (organe.organeParent) {
              organe = organe.organeParent
            }
            originOrganeCodeType = organe.codeType
            originOrganeName = shortNameFromOrgane(organe)
          }
        }
        break
      case "SaisineConseilConstit":
        if (acte.organe !== null) {
          originOrganeCodeType = acte.organe.codeType
          originOrganeName = shortNameFromOrgane(acte.organe)
        }
        break
      default:
        break
    }

    if (acte.codeActe.startsWith("CMP-DEBATS-")) {
      const level1Acte = ancestors[1]
      organeCodeType = level1Acte.organe.codeType
      organeName = shortNameFromOrgane(level1Acte.organe)
      title = "Lecture du texte de la Commission mixte paritaire"
    }

    if (title !== null && title.toLowerCase() === organeName.toLowerCase()) {
      title = null
    }
    // if (rootActe.codeActe === "CMP") {
    //   organeName = "Assemblée et Sénat"
    // } else if (rootActe.codeActe === "PROM") {
    //   // Promulgation de la loi par le Chef de l'État
    //   organeCodeType = "GOUVERNEMENT"
    //   organeName = "Gouvernement"
    //   title = null
    // } else if (acte.codeActe.endsWith("-AVCE")) {
    //   // Avis du Conseil d'État
    //   organeCodeType = "CE"
    //   organeName = "Conseil d'État"
    //   title = null
    // } else if (acte.codeActe.endsWith("-ETI") || acte.codeActe.endsWith("-PROCACC")) {
    //   // "-ETI": By default, "les études d'impact" come from "Gouvernement".
    //   // "-PROCACC": Le Gouvernement déclare l'urgence / engage la procédure accélérée.
    //   organeCodeType = "GOUVERNEMENT"
    //   organeName = "Gouvernement"
    //   title = null
    // }

    // Move documents in acte to documentByUid & context.texteLoiUid
    if (acte.texteAdopte) {
      // Remove acte.texteAdopte => It will be retrieved using documentByUid[acte.texteAdopteRef].
      documentByUid[acte.texteAdopte.uid] = acte.texteAdopte
      assert.strictEqual(acte.texteAdopte.__typename, "TexteLoi")
      context.texteLoiUid = acte.texteAdopte.uid
      delete acte.texteAdopte
    }
    if (acte.texteAssocie) {
      // Remove acte.texteAssocie => It will be retrieved using documentByUid[acte.texteAssocieRef].
      documentByUid[acte.texteAssocie.uid] = acte.texteAssocie
      if (acte.texteAssocie.__typename === "TexteLoi") {
        context.texteLoiUid = acte.texteAssocie.uid
      }
      delete acte.texteAssocie
    }

    const step = {
      actes: ancestors,
      color: colorFromOrganeCodeType(organeCodeType),
      date: acte.dateActe || null,
      leafActe: acte,
      organeName,
      originColor: colorFromOrganeCodeType(originOrganeCodeType),
      originOrganeName,
      rootActe,
      skipRootActe,
      texteLoiUid: context.texteLoiUid,
      title,
      typeActe: acte.__typename,
    }
    steps.push(step)
    if (acte.__typename == "EtudeImpact" && acte.contributionInternaute) {
      // Consultation des internautes
      if (acte.contributionInternaute.dateOuverture) {
        steps.push({
          ...step,
          color: colorFromOrganeCodeType("INTERNET"),
          date: acte.contributionInternaute.dateOuverture,
          organeName: "Internet",
          originColor: step.color,
          originOrganeName: step.organeName,
          title: "Consultation",
          typeActe: `${step.typeActe}.contributionInternaute.dateOuverture`,
        })
      }
      if (acte.contributionInternaute.dateFermeture) {
        steps.push({
          ...step,
          date: acte.contributionInternaute.dateFermeture,
          originColor: colorFromOrganeCodeType("INTERNET"),
          originOrganeName: "Internet",
          title: "Consultation",
          typeActe: `${step.typeActe}.contributionInternaute.dateFermeture`,
        })
      }
    } else if (acte.__typename == "Promulgation" && acte.infoJo) {
      steps.push({
        ...step,
        date: acte.infoJo.dateJo,
        typeActe: "Promulgation.infoJo",
      })
      // Handle "publications rectificatives au Journal officiel"
      for (let infoJo of acte.infoJoRect) {
        steps.push({
          ...step,
          date: infoJo.dateJo,
          infoJo,
          typeActe: "Promulgation.infoJoRect",
        })
      }
    }
  }
}
