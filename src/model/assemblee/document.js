import assert from "assert"
import { JSDOM } from "jsdom"

import config from "../../config"
import { generateDocumentFragments } from "../../graphql/assemblee"
import { assembleeGraphqlClient, textFromFragments } from "../../graphql/clients"
import { unflattenDocument } from "./helpers"
import { GraphqlAgent } from "../data_agents"

const documentQueryText = `
  query Document($date: String!, $uid: String!) {
    document(uid: $uid) {
      ...DocumentFragmentWithDossier
    }
  }
  ${textFromFragments(generateDocumentFragments({ withDossier: true }))}
`
  .trim()
  .replace(/^  /gm, "")

export function newGraphqlAgent(uid) {
  const dateText = new Date().toISOString().split("T")[0]
  return new GraphqlAgent(
    "Document de l'Assemblée",
    config.assembleeApiUrl,
    assembleeGraphqlClient,
    documentQueryText,
    {
      date: dateText,
      uid,
    },
    null,
    function (graphqlData) {
      return unflattenDocument(
        graphqlData
          .data
          .document
      )
    },
  )
}

const levelsClassStart = [
  "a1Tome",
  "a2Partie",
  "a3Livre",
  "a4Titre",
  "a5Chapitre",
  "a6Section",
  "a7Sous-section",
  "a8Paragraphe",
  "a9Article",
]

export async function retrieveTexteLoiAssembleeParsed(fetch, assembleeUrl) {
  const response = await fetch(assembleeUrl)
  const page = await response.text()
  if (!response.ok) {
    return {
      error: { code: response.status, message: response.statusText },
      page,
    }
  }

  // Repair HTML.
  const pageSegments = page.split("</style>")
  assert.strictEqual(pageSegments.length, 2)
  let html = pageSegments[1].trim()
  assert(html.startsWith("<div"))
  assert(html.endsWith("</div>"))
  html = `<html${html.substring(4, html.length - 6)}</html>`

  // Extract subdivisions from HTML.
  const { document } = (new JSDOM(html)).window
  // Find a line in the corpse of the document, and then use its parent as root element.
  let lineElement = document.getElementsByClassName("aLoiTexte")[0]
  if (lineElement === undefined) {
    // Occurs for http://www.assemblee-nationale.fr/15/textes/0003.asp.
    lineElement = document.getElementsByClassName("a9ArticleNum")[0]
    if (lineElement === undefined) {
      return {
        error: { code: -1, message: "Texte de loi au format inconnu" },
        html,
        page,
      }
    }
  }
  const alineasRootDivElement = lineElement.parentElement
  assert.strictEqual(alineasRootDivElement.tagName, "DIV")
  let level = null
  let levels = []
  const subdivisions = []
  let subdivisionAlineasHtml = null
  let subdivisionAlineasText = null
  let subdivisionHeadersHtml = null
  let subdivisionHeadersText = null
  for (let alineaPElement of alineasRootDivElement.getElementsByTagName("p")) {
    let alineaClass = alineaPElement.getAttribute("class")

    // Try to repair a few errors.
    if (alineaClass === "aNormalWeb") {
      const alineaText = alineaPElement.textContent.trim()
      if (alineaText.toLowerCase().startsWith("article ")) {
        // This error occurs in Article 1er of
        // http://www.assemblee-nationale.fr/15/textes/0143.asp
        alineaClass = "a9Article"
      }
    } else if (alineaClass === "a4titreintit1") {
      const alineaText = alineaPElement.textContent.trim()
      if (alineaText.toLowerCase() === "proposition de loi") {
        // This error occurs in Article 1er of
        // http://www.assemblee-nationale.fr/15/textes/0232.asp
        continue
      }
    }

    if (alineaClass === null || alineaClass === "aLoiTexte") {
      // Examples with alineaClass === null:
      // * Texte non pastillé (par exemple "Article 2 bis (nouveau)"
      //   de http://www.assemblee-nationale.fr/15/textes/1353.asp
      // * Texte pastillé et non pastillé
      //   de http://www.assemblee-nationale.fr/15/textes/0003.asp

      // Remove "pastilles".
      const pastilleElements = alineaPElement.getElementsByClassName("aEloiPastille")
      for (let pastilleElement of pastilleElements) {
        pastilleElement.remove()
      }

      const alineaText = alineaPElement.textContent.trim()
      if (alineaText) {
        subdivisionAlineasHtml.push(alineaPElement.outerHTML)
        subdivisionAlineasText.push(alineaText)
        level = null
      }
    } else {
      for (let [index, classStart] of levelsClassStart.entries()) {
        if (alineaClass.startsWith(classStart)) {
          if (index !== level) {
            const namedElement = [...alineaPElement.children].find(childElement => {
              if (childElement.tagName === "A") {
                const name = childElement.getAttribute("name")
                if (name !== null && name.startsWith("D_")) {
                  return true
                }
              }
              return false
            })
            if (namedElement !== undefined) {
              const divisionId = namedElement.getAttribute("name")
              assert.notStrictEqual(divisionId, null)
              level = index
              while (levels.length > 0 && level < levels[levels.length - 1]) {
                levels.pop()
              }
              if (levels.length === 0 || level > levels[levels.length - 1]) {
                levels.push(level)
              }
              subdivisionAlineasHtml = []
              subdivisionAlineasText = []
              subdivisionHeadersHtml = []
              subdivisionHeadersText = []
              subdivisions.push({
                html: {
                  alineas: subdivisionAlineasHtml,
                  headers: subdivisionHeadersHtml,
                },
                id: divisionId,
                level: level + 1,
                relativeLevel: levels.length,
                text: {
                  alineas: subdivisionAlineasText,
                  headers: subdivisionHeadersText,
                },
              })
            }
          }
          break
        }
      }
      const alineaText = alineaPElement.textContent.trim()
      if (alineaText) {
        if (subdivisionAlineasHtml.length === 0) {
          subdivisionHeadersHtml.push(alineaPElement.outerHTML)
          subdivisionHeadersText.push(alineaText)
        } else {
          // The header belongs to the body of the article (occurs when an article modifies a header).
          subdivisionAlineasHtml.push(alineaPElement.outerHTML)
          subdivisionAlineasText.push(alineaText)
        }
      }
    }
  }

  return {
    error: null,
    html,
    page,
    subdivisions,
    url: assembleeUrl,
  }
}