import config from "../../config"
import { generateTexteLegFragments } from "../../graphql/assemblee"
import { assembleeGraphqlClient, textFromFragments } from "../../graphql/clients"
import { unflattenTexteLeg } from "./helpers"
import { GraphqlAgent } from "../data_agents"

const amendementsTexteLegislatifQueryText = `
  query AmendementsTexteLegislatif($date: String!, $uid: String!) {
    amendementsTexteLegislatif(uid: $uid) {
      ...TexteLegFragment
    }
  }
  ${textFromFragments(generateTexteLegFragments())}
`
  .trim()
  .replace(/^  /gm, "")

export function newGraphqlAgent(uid) {
  const dateText = new Date().toISOString().split("T")[0]
  return new GraphqlAgent(
    "Amendements à un texte législatif de l'Assemblée",
    config.assembleeApiUrl,
    assembleeGraphqlClient,
    amendementsTexteLegislatifQueryText,
    {
      date: dateText,
      uid,
    },
    null,
    function (graphqlData) {
      return unflattenTexteLeg(
        graphqlData
          .data
          .amendementsTexteLegislatif
      )
    },
  )
}
