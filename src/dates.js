import format from "date-fns/format"
import frLocale from "date-fns/locale/fr"
import parse from "date-fns/parse"

export function displayDateFromEpoch(epoch) {
  const date = new Date(epoch * 1000)
  let dateString = format(date, "dddd D MMMM YYYY HH[h]mm", { locale: frLocale })
  if (date.getUTCHours() === 0 && date.getUTCMinutes() === 0) {
    dateString = dateString.substring(0, dateString.length - 6)
  }
  return dateString
}

export function displayDateFromString(dateString) {
  const date = parse(dateString)
  dateString = format(date, "dddd D MMMM YYYY HH[h]mm", { locale: frLocale })
  if (dateString.endsWith(" 00h00")) {
    dateString = dateString.substring(0, dateString.length - 6)
  }
  return dateString
}

export function displayDateWithoutDayOfWeekFromEpoch(epoch) {
  const date = new Date(epoch * 1000)
  let dateString = format(date, "D MMMM YYYY HH[h]mm", { locale: frLocale })
  if (date.getUTCHours() === 0 && date.getUTCMinutes() === 0) {
    dateString = dateString.substring(0, dateString.length - 6)
  }
  return dateString
}

export function displayDateWithoutDayOfWeekFromString(dateString) {
  const date = parse(dateString)
  dateString = format(date, "D MMMM YYYY HH[h]mm", { locale: frLocale })
  if (dateString.endsWith(" 00h00")) {
    dateString = dateString.substring(0, dateString.length - 6)
  }
  return dateString
}

export function displayTimeFromEpoch(epoch) {
  const isoDate = new Date(epoch * 1000)
  const localDate = new Date((epoch + isoDate.getTimezoneOffset() * 60) * 1000)
  return format(localDate, "H[h]mm", { locale: frLocale })
}

export function displayTimeFromString(dateString) {
  const date = parse(dateString)
  return format(date, "H[h]mm", { locale: frLocale })
}
