import originalSlugify from "slug"

const slugifyCharmap = { ...originalSlugify.defaults.charmap, "'": " ", "@": " ", ".": " " }

export function buildSearchUrl(url, { limit = 10, offset = 0, term = "" }) {
  let query = []
  if (term) {
    query.push(`q=${encodeURIComponent(term).replace("(", " ").replace(")", " ")}`)
  }
  if (offset !== 0) {
    query.push(`offset=${offset}`)
  }
  if (limit !== 10) {
    query.push(`limit=${limit}`)
  }
  query = query.join("&")
  if (query) {
    query = (url.includes("?") ? "&" : "?") + query
  }
  return url + query
}

export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

export function slugify(string, replacement) {
  const options = {
    charmap: slugifyCharmap,
    mode: "rfc3986",
  }
  if (replacement) {
    options.replacement = replacement
  }
  return originalSlugify(string, options)
}

export function uncapitalizeFirstLetter(string) {
  return string.charAt(0).toLowerCase() + string.slice(1)
}
