import config from "../../../../config"
import { generateActeurFragments, generateReunionFragments } from "../../../../graphql/assemblee"
import { assembleeGraphqlClient, textFromFragments } from "../../../../graphql/clients"
import { GraphqlAgent, GraphqlMultiAgent } from "../../../../model/data_agents"

const agendaDeputeQueryText = `
  query agenda($dateDebut: String!, , $dateFin: String!, $uid: String!) {
    agenda(acteurUid: $uid, dateDebut: $dateDebut, dateFin: $dateFin) {
      ...ReunionFragment
    }
  }
  ${textFromFragments(generateReunionFragments({ withDossier: true }))}
`
  .trim()
  .replace(/^  /gm, "")

const deputeQueryText = `
  query acteur($date: String!, $uid: String!) {
    acteur(uid: $uid) {
      ...ActeurFragmentWithCommissionGroupeMandats
    }
  }
  ${textFromFragments(generateActeurFragments({ withCommission: true, withGroupe: true, withMandats: true }))}
`
  .trim()
  .replace(/^  /gm, "")

export function newGraphqlAgent(uid) {
  const dateText = new Date().toISOString().split("T")[0]
  const month = new Date().getMonth()
  const dateDebut = new Date()
  dateDebut.setMonth(month - 1)
  const dateFin = new Date()
  dateFin.setMonth(month + 1)
  return new GraphqlMultiAgent({
    agendaDeputeAgent: new GraphqlAgent(
      "Agenda de l'Assemblée pour le député",
      config.assembleeApiUrl,
      assembleeGraphqlClient,
      agendaDeputeQueryText,
      {
        dateDebut: dateDebut.toISOString(),
        dateFin: dateFin.toISOString(),
        uid,
      }
    ),
    deputeAgent: new GraphqlAgent(
      "Informations de l'Assemblée sur le député",
      config.assembleeApiUrl,
      assembleeGraphqlClient,
      deputeQueryText,
      {
        date: dateText,
        uid,
      }
    ),
  })
}
