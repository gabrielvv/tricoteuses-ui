import config from "../../../config"
import { generateActeurFragments, generateMandatFragments } from "../../../graphql/assemblee"
import { assembleeGraphqlClient, textFromFragments } from "../../../graphql/clients"
import { GraphqlAgent, GraphqlMultiAgent } from "../../../model/data_agents"

const deputeQueryText = `
  query acteur($date: String!, $uid: String!) {
    acteur(uid: $uid) {
      ...ActeurFragmentWithCommissionGroupeMandats
      mandats(date: $date) {
        ...MandatFragment
      }
    }
    fonctionsOrganes {
      libelle
      libellePluriel
      typesOrganes {
        code
        libelle
        libellePluriel
      }
    }
  }
  ${textFromFragments({
    ...generateActeurFragments({ withCommission: true, withGroupe: true, withMandats: true }),
    ...generateMandatFragments(),
  })}
`
  .trim()
  .replace(/^  /gm, "")

export function newGraphqlAgent(uid) {
  const dateText = new Date().toISOString().split("T")[0]
  return new GraphqlMultiAgent({
    deputeAgent: new GraphqlAgent(
      "Informations de l'Assemblée sur le député",
      config.assembleeApiUrl,
      assembleeGraphqlClient,
      deputeQueryText,
      {
        date: dateText,
        uid,
      }
    ),
  })
}
