import config from "../../config"
import { generateActeurFragments } from "../../graphql/assemblee"
import { assembleeGraphqlClient, textFromFragments } from "../../graphql/clients"
import { GraphqlAgent, GraphqlMultiAgent } from "../../model/data_agents"

const deputesQueryText = `
  query deputes($date: String!) {
    deputes(date: $date) {
      ...ActeurFragmentWithCommissionGroupe
    }
  }
  ${textFromFragments(generateActeurFragments({ withCommission: true, withGroupe: true, withMandats: false }))}
`
  .trim()
  .replace(/^  /gm, "")

export function newGraphqlAgent() {
  const dateText = new Date().toISOString().split("T")[0]
  return new GraphqlMultiAgent({
    deputesAgent: new GraphqlAgent(
      "Informations de l'Assemblée sur les députés",
      config.assembleeApiUrl,
      assembleeGraphqlClient,
      deputesQueryText,
      {
        date: dateText,
      }
    ),
  })
}
