import config from "../../../../config"
import { senatGraphqlClient } from "../../../../graphql/clients"
import { GraphqlAgent, GraphqlMultiAgent } from "../../../../model/data_agents"

const qua = `
  {
    quacod
    qualic
    quaabr
    quaabrplu
  }
`
  .trim()
  .replace(/^  /gm, "")

const typAut = `
  {
    typautcod
    typautlib
  }
`
  .trim()
  .replace(/^  /gm, "")

const auteur = `
  {
    autcod
    quacod
    qua ${qua.replace(/^/gm, "  ".repeat(2)).trim()}
    typautcod
    typaut ${typAut.replace(/^/gm, "  ".repeat(2)).trim()}
    nomuse
    prenom
    nomtec
    autmat
    grpapp
    grprat
    autfct
    datdeb
    datfin
    senfem
  }
`
  .trim()
  .replace(/^  /gm, "")

const debat = `
  {
    datsea
    debsyn
    autinc
    deburl
    numero
    estcongres
    libspec
    etavidcod
  }
`
  .trim()
  .replace(/^  /gm, "")

const rolSig = `
  {
    signataire
    rolsiglib
  }
`
  .trim()
  .replace(/^  /gm, "")

const ecr = `
  {
    rapcod
    ecrnumtri
    autcod
    aut ${auteur.replace(/^/gm, "  ".repeat(2)).trim()}
    texcod
    ecrnum
    typedoc
    signataire
    rolsig ${rolSig.replace(/^/gm, "  ".repeat(2)).trim()}
    docidt
    ecrqua
  }
`
  .trim()
  .replace(/^  /gm, "")

const typOrg = `
  {
    typorgcod
    typorglib
    typorgurl
    typorgtitens
    typorgvid
    typorgord
  }
`
  .trim()
  .replace(/^  /gm, "")

const org = `
  {
    orgcod
    typorgcod
    typorg ${typOrg.replace(/^/gm, "  ".repeat(2)).trim()}
    orgnom
    orgliblon
    codass
    orglibaff
    orgord
    orgurl
    orglibcou
    orgDe
    urltra
    inttra
    orgdatdebcop
    orgdatfincop
    orgnomcouv
    senorgcod
    htmlColor
    orgdatdeb
    orgdatfin
    orggen
  }
`
  .trim()
  .replace(/^  /gm, "")

const scr = `
  {
    sesann
    scrnum
    code
    scrint
    scrdat
    scrpou
    scrcon
    scrvot
    scrsuf
    scrvotsea
    scrsufsea
    scrpousea
    scrconsea
    scrmaj
    scrmajsea
  }
`
  .trim()
  .replace(/^  /gm, "")

const dossierLegislatifQueryResponseText = `
  {
    loicod
    typloi {
      typloicod
      typloilib
      groupe
      typloiden
      typloigen
      typloitit
      typloidenplu
      typloide
      typloiabr
    }
    etaloi {
      etaloicod
      etaloilib
    }
    deccoccod
    deccoc {
      deccoccod
      deccoclib
    }
    numero
    signet
    loient
    motclef
    loitit
    loiint
    urgence
    urlJo
    loinumjo
    loidatjo
    dateLoi
    loititjo
    urlJo2
    loinumjo2
    loidatjo2
    deccocurl
    numDecision
    dateDecision
    loicodmai
    loinoudelibcod
    motionloiorigcod
    objet
    urlOrdonnance
    saisineDate
    saisinePar
    loidatjo3
    loinumjo3
    urlJo3
    urlAn
    urlPresart
    signetalt
    orgcod
    org ${org.replace(/^/gm, "  ".repeat(2)).trim()}
    doscocurl
    loiintori
    proaccdat
    proaccoppdat
    retproaccdat
    lectures {
      lecidt
      loicod
      typlec {
        typleccod
        typleclib
        typlecord
      }
      leccom
      lecass {
        lecassidt
        lecidt
        ass {
          codass
          libass
        }
        ordreass
        sesann
        ptlnum
        ptlurl
        ptlnumcpl
        ptlnot
        ptlurl2
        ptlnot2
        ptlurl3
        ptlnot3
        ptlnumcpl2
        ptlnumcpl3
        lecassame
        lecassameses
        orgcod
        org ${org.replace(/^/gm, "  ".repeat(4)).trim()}
        loiintmod
        reucom
        debatsurl
        depotOnly
        lecassamedat
        lecassamecomdat
        orippr
        libppr
        sesppr
        ptlurlcom
        aliasppr
        lecassamecom
        lecassamesescom
        ptlnumcom
        auds {
          audcle
          lecassidt
          auddat
          audtit
          audurl
          orgcod
          org ${org.replace(/^/gm, "  ".repeat(5)).trim()}
        }
        datesSeances {
          lecidt
          dateS
          code
          statut
          debat ${debat.replace(/^/gm, "  ".repeat(5)).trim()}
          scrs ${scr.replace(/^/gm, "  ".repeat(5)).trim()}
        }
        lecassrap {
          lecassidt
          rap {
            rapcod
            sesann
            denrap {
              coddenrap
              typraprap
              libdenrap
              ordre
              denrapmin
              denraptit
              denrapstymin
              gencod
              solnatrapcod
            }
            typurl
            blecod
            rapnum
            raptom
            rapfac
            rapann
            rapvol
            raptitcou
            raptil
            rapurl
            url2
            url3
            url4
            url2txt
            url3txt
            url4txt
            dateDepot
            prix
            rapnuman
            numerobis
            rapsoustit
            rapdatsea
            depotOnly
            rapres
            forpubcod
            docatts {
              docattcle
              rapcod
              typattcod
              typatt {
                typattcod
                typattlib
              }
              docatturl
            }
            ecrs ${ecr.replace(/^/gm, "  ".repeat(6)).trim()}
            orgs ${org.replace(/^/gm, "  ".repeat(6)).trim()}
          }
          lecassrapord
        }
        textes {
          texcod
          oritxt {
            oritxtcod
            oritxtlib
            oriordre
            codass
            oritxtlibfem
            oritxtado
            oritxtorg
            oritxtmod
          }
          typtxtcod
          typurl
          lecassidt
          sesann
          orgcod
          org ${org.replace(/^/gm, "  ".repeat(5)).trim()}
          texnum
          texurl
          url2
          url3
          url4
          url2txt
          url3txt
          url4txt
          txtoritxtdat
          prix
          numerobis
          texdatsea
          reserveComspe
          datrejetDiscImmediate
          texace
          ecrs ${ecr.replace(/^/gm, "  ".repeat(5)).trim()}
        }
      }
    }
  }
`
  .trim()
  .replace(/^  /gm, "  ".repeat(2))

const dossierLegislatifQueryText = `
  query DossierLegislatif($signet: String!) {
    dossierLegislatifParSignet(signet: $signet) ${dossierLegislatifQueryResponseText}
  }
`
  .trim()
  .replace(/^  /gm, "")

export function newGraphqlAgent(signet) {
  return new GraphqlMultiAgent({
    dossierLegislatifAgent: new GraphqlAgent(
      "Dossier législatif du Sénat",
      config.senatApiUrl,
      senatGraphqlClient,
      dossierLegislatifQueryText,
      {
        signet,
      }
    ),
  })
}
