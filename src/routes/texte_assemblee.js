import fetch from "node-fetch"
import url from "url"

import { retrieveTexteLoiAssembleeParsed } from "../model/assemblee/document"
import { validateNonEmptyTrimmedString } from "../validators/core"

export async function post(req, res) {
  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in form:\n${JSON.stringify(body, null, 2)}\n\nError:\n${JSON.stringify(error, null, 2)}`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body for form",
          },
        },
        null,
        2
      )
    )
  }

  const assembleeUrl = url.resolve("http://www.assemblee-nationale.fr/", body.url)
  const texteLoiAssembleeParsed = await retrieveTexteLoiAssembleeParsed(fetch, assembleeUrl)

  const { error, page } = texteLoiAssembleeParsed
  if (error) {
    console.error(
      `Error while getting page "${assembleeUrl}":\n\nError:\n${JSON.stringify(error, null, 2)}`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            details: error,
            message: `Retrieval of HTML page ${assembleeUrl} failed`,
            page,
          },
        },
        null,
        2
      )
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(texteLoiAssembleeParsed, null, 2))
}

function validateBody(body) {
  if (body === null || body === undefined) {
    return [body, "Missing body"]
  }
  if (typeof body !== "object") {
    return [body, `Expected an object, got ${typeof body}`]
  }

  body = {
    ...body,
  }
  const remainingKeys = new Set(Object.keys(body))
  const errors = {}

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [body, Object.keys(errors).length === 0 ? null : errors]
}
