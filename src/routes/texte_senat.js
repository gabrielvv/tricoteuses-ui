import assert from "assert"
import { JSDOM } from "jsdom"
import fetch from "node-fetch"
import url from "url"

import { validateArray, validateNonEmptyTrimmedString } from "../validators/core"

const embedHtmlRegExp = /<\?xml .*$/m

export async function post(req, res) {
  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in form:\n${JSON.stringify(body, null, 2)}\n\nError:\n${JSON.stringify(error, null, 2)}`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body for form",
          },
        },
        null,
        2
      )
    )
  }

  const senatUrl = url.resolve("https://www.senat.fr/leg/", body.url)
  const response = await fetch(senatUrl)
  const page = await response.text()
  if (!response.ok) {
    const error = { code: response.status, message: response.statusText }
    console.error(
      `Error while getting page "${senatUrl}":\n\nError:\n${JSON.stringify(error, null, 2)}`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            details: error,
            message: `Retrieval of HTML page ${senatUrl} failed`,
            page,
          },
        },
        null,
        2
      )
    )
  }

  const match = page.match(embedHtmlRegExp)
  if (match === null) {
    console.error(
      `Page "${senatUrl}" doesn't seem to embed another HTML page`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: `Page "${senatUrl}" doesn't seem to embed another HTML page`,
            page,
          },
        },
        null,
        2
      )
    )
  }
  const embedHtml = match[0]

  // Extract subdivisions from HTML.
  const { document } = (new JSDOM(embedHtml)).window
  const alineasRootDivElement = document.getElementById("formCorrection:tableauComparatif")
  assert.strictEqual(alineasRootDivElement.tagName, "DIV")
  const subdivisionsIds = new Set(body.ids)
  const subdivisionById = {}
  let subdivisionAlineasHtml = null
  let subdivisionAlineasText = null
  let subdivisionHeadersHtml = null
  let subdivisionHeadersText = null
  for (let tbodyElement of alineasRootDivElement.getElementsByTagName("tbody")) {
    for (let trElement of tbodyElement.children) {
      // Try to retrieve a subdivision ID.
      const pastilleTdElement = trElement.children[0]
      const pastilleDivElements = [...pastilleTdElement.children]
      assert.strictEqual(pastilleDivElements.length, 1)
      const pastilleDivElement = pastilleDivElements[0]
      assert.strictEqual(pastilleDivElement.tagName, "DIV")
      const pastillePElements = [...pastilleDivElement.children]
      assert.strictEqual(pastillePElements.length, 1)
      const pastillePElement = pastillePElements[0]
      assert.strictEqual(pastillePElement.tagName, "P")
      const alineaId = pastillePElement.getAttribute("id")
      if (alineaId !== null && subdivisionsIds.has(alineaId)) {
        subdivisionAlineasHtml = []
        subdivisionAlineasText = []
        subdivisionHeadersHtml = []
        subdivisionHeadersText = []
        subdivisionById[alineaId] = {
          html: {
            alineas: subdivisionAlineasHtml,
            headers: subdivisionHeadersHtml,
          },
          text: {
            alineas: subdivisionAlineasText,
            headers: subdivisionHeadersText,
          },
        }
      }
      if (subdivisionAlineasHtml === null) {
        continue
      }
      const alineaTdElement = trElement.children[2]
      const alineaDivElements = [...alineaTdElement.children]
      assert.strictEqual(alineaDivElements.length, 1)
      const alineaDivElement = alineaDivElements[0]
      assert.strictEqual(alineaDivElement.tagName, "DIV")
      const alineaPElements = [...alineaDivElement.children]
      for (let alineaPElement of alineaPElements) {
        assert.strictEqual(alineaPElement.tagName, "P")
        const style = alineaPElement.getAttribute("style")
        assert.notStrictEqual(style, null)
        if (style.includes("text-align: center")) {
          subdivisionHeadersHtml.push(alineaPElement.outerHTML)
          subdivisionHeadersText.push(alineaPElement.textContent.trim())
        } else {
          subdivisionAlineasHtml.push(alineaPElement.outerHTML)
          subdivisionAlineasText.push(alineaPElement.textContent.trim())
        }
      }
    }
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({
    html: embedHtml,
    subdivisions: subdivisionById,
    url: senatUrl,
  }, null, 2))
}

function validateBody(body) {
  if (body === null || body === undefined) {
    return [body, "Missing body"]
  }
  if (typeof body !== "object") {
    return [body, `Expected an object, got ${typeof body}`]
  }

  body = {
    ...body,
  }
  const remainingKeys = new Set(Object.keys(body))
  const errors = {}

  {
    const key = "ids"
    remainingKeys.delete(key)
    const [value, error] = validateArray(validateNonEmptyTrimmedString)(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [body, Object.keys(errors).length === 0 ? null : errors]
}
