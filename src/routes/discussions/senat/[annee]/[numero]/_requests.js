import config from "../../../../../config"
import { senatGraphqlClient } from "../../../../../graphql/clients"
import { GraphqlAgent, GraphqlMultiAgent } from "../../../../../model/data_agents"

const comAmeliFragment = `
  fragment ComAmeliFragment on ComAmeli {
    entid
    cod
    lib
    lil
    spc
    codint
    tri
  }
`
  .trim()
  .replace(/^  /gm, "")

const discussionQueryResponseText = `
  {
    id
    natid
    nat {
      id
      lib
    }
    lecid
    sesinsid
    sesins {
      id
      typid
      ann
      lil
    }
    sesdepid
    sesdep {
      id
      typid
      ann
      lil
    }
    fbuid
    num
    int
    inl
    datdep
    urg
    dis
    secdel
    loifin
    loifinpar
    txtamd
    datado
    numado
    txtexa
    pubdellim
    numabs
    libdelim
    libcplnat
    doslegsignet
    proacc
    txttyp
    ordsnddelib
    txtetaid
    fusderid
    fusder
    fusderord
    fusdertyp
    amds {
      id
      subid
      amdperid
      motid
      etaid
      nomentid
      noment {
        id
        typ
        act
        ent {
          ...EntUnionFragment
        }
      }
      sorid
      sor {
        id
        lib
        cod
        typ
      }
      avcid
      avc {
        id
        lib
        cod
      }
      avgid
      avg {
        id
        lib
        cod
      }
      irrid
      irr {
        id
        lib
        cod
        libirr
        art
        lilmin
        par
      }
      txtid
      opmid
      ocmid
      ideid
      discomid
      num
      rev
      nivrec {
        num
        lib
      }
      typ
      dis
      obj
      datdep
      obs
      ord
      autext
      subpos
      mot
      numabs
      subidder
      libgrp
      alinea
      accgou
      colleg
      typrectid
      islu
      motposexa
      amdsens {
        amdid
        senid
        sen {
          ...SenAmeliFragment
        }
        rng
        qua
        nomuse
        prenomuse
        hom
        grpid
        grp {
          ...GrpPolAmeliFragment
        }
      }
    }
    sais {
      id
      txtid
      comid
      sesid
      numrap
      saityp
      saisens {
        id
        senid
        sen {
          ...SenAmeliFragment
        }
        ord
      }
    }
    subs {
      id
      txtid
      merid
      typid
      typ {
        id
        lib
      }
      lic
      lib
      pos
      sig
      posder
      prires
      dupl
      subamd
      sorid
      txtidder
      style
      islec
    }
    texte {
      texcod
      oritxtcod
      typtxtcod
      typurl
      lecassidt
      lecass {
        lecassidt
        lecidt
        codass
        ordreass
        sesann
        ptlnum
        ptlurl
        ptlnumcpl
        ptlnot
        ptlurl2
        ptlnot2
        ptlurl3
        ptlnot3
        ptlnumcpl2
        ptlnumcpl3
        lecassame
        lecassameses
        orgcod
        loiintmod
        reucom
        debatsurl
        depotOnly
        lecassamedat
        lecassamecomdat
        orippr
        libppr
        sesppr
        ptlurlcom
        aliasppr
        lecassamecom
        lecassamesescom
        ptlnumcom
      }
      sesann
      orgcod
      texnum
      texurl
      url2
      url3
      url4
      url2txt
      url3txt
      url4txt
      txtoritxtdat
      prix
      numerobis
      texdatsea
      reserveComspe
      datrejetDiscImmediate
      texace
    }
  }
`
  .trim()
  .replace(/^  /gm, "  ".repeat(2))

const discussionQueryText = `
  query Discussion($ann: Int!, $num: String!) {
    discussion(ann: $ann, num: $num) ${discussionQueryResponseText}
  }
`
  .trim()
  .replace(/^  /gm, "")

const entUnionFragment = `
  fragment EntUnionFragment on EntUnion {
    ... on Cab {
      entid
      codint
      lilOptional
    }
    ... on ComAmeli {
      ...ComAmeliFragment
    }
    ... on GrpPolAmeli {
      ...GrpPolAmeliFragment
    }
    ... on Gvt {
      entid
      nom
      pre
      qua
      tit
    }
    ... on SenAmeli {
      ...SenAmeliFragment
    }
  }
`
  .trim()
  .replace(/^  /gm, "  ".repeat(2))

const grpPolAmeliFragment = `
  fragment GrpPolAmeliFragment on GrpPolAmeli {
    entid
    cod
    libcou
    lilcou
    codint
    tri
  }
`
  .trim()
  .replace(/^  /gm, "")

const senAmeliFragment = `
  fragment SenAmeliFragment on SenAmeli {
    entid
    grpid
    grp {
      ...GrpPolAmeliFragment
    }
    comid
    com {
      ...ComAmeliFragment
    }
    comspcid
    comspc {
      ...ComAmeliFragment
    }
    mat
    qua
    nomuse
    prenomuse
    nomtec
    hom
    app
    ratt
    nomusemin
    senfem
    photo {
      chemin
      cheminMosaique
      hauteur
      largeur
      xMosaique
      yMosaique
    }
  }
`
  .trim()
  .replace(/^  /gm, "")

export function newGraphqlAgent(ann, num) {
  return new GraphqlMultiAgent({
    discussionAgent: new GraphqlAgent(
      "Discussion",
      config.senatApiUrl,
      senatGraphqlClient,
      discussionQueryText
        + "\n\n" + comAmeliFragment
        + "\n\n" + entUnionFragment
        + "\n\n" + grpPolAmeliFragment
        + "\n\n" + senAmeliFragment,
      {
        ann,
        num,
      }
    ),
  })
}
