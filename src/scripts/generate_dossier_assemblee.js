import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

import { generateDossiersAssemblee } from "../generators/assemblee/dossiers"

const optionsDefinitions = [
  { defaultOption: true, name: "uid", type: String, help: 'UID of Assemblée "dossier" to generate' },
]
const options = commandLineArgs(optionsDefinitions)

generateDossiersAssemblee(fs, path, options.uid)
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })
