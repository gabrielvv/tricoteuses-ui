import fs from "fs-extra"
import path from "path"

import { generateDossiersAssemblee } from "../generators/assemblee/dossiers"

generateDossiersAssemblee(fs, path)
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })
