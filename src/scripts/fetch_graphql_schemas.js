// Cf https://www.apollographql.com/docs/react/recipes/fragment-matching.html

import fetch from "cross-fetch"
import fs from "fs"
import path from "path"
import resolveUrl from "url-resolve"

import config from "../config"

const fragmentTypesDir = path.join("src", "graphql", "fragment_types")
if (!fs.existsSync(fragmentTypesDir)) {
  fs.mkdirSync(fragmentTypesDir)
}

for (let { configName, filename } of [
  // {
  //   configName: "apiUrl",
  //   filename: "api.json",
  // },
  {
    configName: "assembleeApiUrl",
    filename: "assemblee_api.json",
  },
  {
    configName: "senatApiUrl",
    filename: "senat_api.json",
  },
]) {
  const graphqlServerUrl = resolveUrl(config[configName], "/graphql")
  console.log(`Retrieving GraphQL fragment types from ${graphqlServerUrl}`)
  fetch(graphqlServerUrl, {
    body: JSON.stringify(
      {
        variables: {},
        // operationName: "",
        query: `
        {
          __schema {
            types {
              kind
              name
              possibleTypes {
                name
              }
            }
          }
        }
      `,
      },
      null,
      2
    ),
    headers: { "Content-Type": "application/json; charset=utf-8" },
    method: "POST",
  })
    .then(result => result.json())
    .then(result => {
      // Filter out any type information unrelated to unions or interfaces.
      const filteredData = result.data.__schema.types.filter(type => type.possibleTypes !== null)
      result.data.__schema.types = filteredData
      fs.writeFile(path.join(fragmentTypesDir, filename), JSON.stringify(result.data, null, 2), err => {
        if (err) {
          console.error("Error writing fragmentTypes file", err)
        } else {
          console.log("Fragment types successfully extracted!")
        }
      })
    })
}
