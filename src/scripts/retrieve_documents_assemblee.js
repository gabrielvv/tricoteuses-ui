import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import gql from "graphql-tag"
import fetch from "node-fetch"
import path from "path"

import { generateClassificationFragments } from "../graphql/assemblee"
import { assembleeGraphqlClient, textFromFragments } from "../graphql/clients"
import { urlFromDocument } from "../model/assemblee/helpers"
import { retrieveTexteLoiAssembleeParsed } from "../model/assemblee/document"

const optionsDefinitions = [
  { defaultOption: true, name: "uid", type: String, help: "UID of first Assemblée document to retrieve" },
]
const options = commandLineArgs(optionsDefinitions)

export async function retrieveDocuments(fetch, fs, path, uidsToSkip) {
  const graphqlData = await assembleeGraphqlClient.query({
    query: gql`
      query TypenamesAndUidsDocuments {
        documents {
          __typename
          ... on AccordInternational {
            uid
          }
          ... on AvisConseilEtat {
            uid
          }
          ... on DocumentEtudeImpact {
            uid
          }
          ... on RapportParlementaire {
            uid
          }
          ... on TexteLoi {
            classification {
              ...ClassificationFragment
            }
            uid
          }
        }
      }
      ${textFromFragments(generateClassificationFragments())}
    `,
    variables: {},
  })
  const documents = graphqlData
    .data
    .documents
    .filter(document => !uidsToSkip || !uidsToSkip.has(document.uid))

  const textesLois = documents
    .filter(document => document.__typename === "TexteLoi")
    .filter(document => document.uid.startsWith("PIONAN") || document.uid.startsWith("PRJLAN"))
    .map(document => {
      return {
        ...document,
        rawDocumentAssembleeUrl: urlFromDocument(document, "raw-html"),
      }
    })
    .sort((a, b) => a.rawDocumentAssembleeUrl.localeCompare(b.rawDocumentAssembleeUrl))
  const firstUid = options.uid
  let skip = !!firstUid
  for (let texteLoi of textesLois) {
    if (skip) {
      if (texteLoi.uid === firstUid) {
        skip = false
      } else {
        continue
      }
    }
    console.log(texteLoi.uid, texteLoi.rawDocumentAssembleeUrl)
    const texteLoiParsed = await retrieveTexteLoiAssembleeParsed(fetch, texteLoi.rawDocumentAssembleeUrl)
    const { error } = texteLoiParsed
    if (error) {
      console.error(
        `Error while getting page "${texteLoi.rawDocumentAssembleeUrl}" (uid: ${texteLoi.uid}):\n\nError:\n${
          JSON.stringify(error, null, 2)}`
      )
      continue
    }
  }
  return documents
}

retrieveDocuments(fetch, fs, path, new Set())
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })
