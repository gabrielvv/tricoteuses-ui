import fs from "fs-extra"
import path from "path"

import { generateDossiersActuels  } from "../model/generators/dossier_actuels"

generateDossiersActuels(fs, path)
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })
