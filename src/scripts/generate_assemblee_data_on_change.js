import fs from "fs-extra"
import gql from "graphql-tag"
import path from "path"

import { createGraphqlClientForAssemblee } from "../graphql/clients"
import { generateDossierAssemblee } from "../generators/assemblee/dossier"
import { generateDossiersAssemblee } from "../generators/assemblee/dossiers"
import { generateDossiersActuels } from "../generators/dossiers_actuels"

const statutQuery = gql`
  query statut {
    statut {
      dateMiseAJourDonnees
    }
  }
`

async function main() {
  // Cf https://stackoverflow.com/questions/47879016/how-to-disable-cache-in-apollo-link-or-apollo-client
  const assembleeGraphqlClient = createGraphqlClientForAssemblee({
    query: {
      fetchPolicy: "no-cache",
      errorPolicy: "all",
    },
  })
  let latestDateMiseAJourDonnees = null
  while (true)  {
    try {
      const graphqlData = await assembleeGraphqlClient.query({
        query: statutQuery,
      })
      const dateMiseAJourDonnees = graphqlData
        .data
        .statut
        .dateMiseAJourDonnees
      if (dateMiseAJourDonnees !== latestDateMiseAJourDonnees) {
        latestDateMiseAJourDonnees = dateMiseAJourDonnees
        console.log(`Les données du serveur GraphQL de l'Assemblée ont été mises à jour : ${
          latestDateMiseAJourDonnees}`)
        const dossiersActuelsViewData = await generateDossiersActuels(fs, path)
        const dossiersActuelsAssembleeUids = dossiersActuelsViewData.dossiersActuelsKnitted
          .filter(dossierKnitted => dossierKnitted.assemblee !== null)
          .map(dossierKnitted => dossierKnitted.assemblee.uid)
        // First regenerate Assemblée "dossiers actuels", because they are the most likely to have changed.
        for (let uid of dossiersActuelsAssembleeUids) {
          await generateDossierAssemblee(fs, path, uid)
        }
        // Then regenerate the remaining Assemblée "dossiers".
        await generateDossiersAssemblee(fs, path, new Set(dossiersActuelsAssembleeUids))
        // TODO: Also generate data for Sénat "dossiers".
      }
    } catch (e) {
      if (e.networkError) {
        console.log(`Le serveur des données GraphQL de l'Assemblée ne répond pas : ${new Date()}`)
      } else {
        throw e
      }
      // if (e instanceof NetworkError)
    }
    await sleep(60000)
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

main()
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })
