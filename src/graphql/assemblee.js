const codier = `
  {
    famCode
    libelle
  }
`
  .trim()
  .replace(/^  /gm, "  ".repeat(6))

const fragmentsCache = {}

function documentShared(withDossier = false) {
  const dossier = withDossier
    ?"\n    dossier {\n      ...DossierParlementaireFragment\n    }"
    : ""
  return `
    auteurs {
      ...AuteurLegFragment
    }
    classification {
      ...ClassificationFragment
    }
    correction {
      niveauCorrection
      typeCorrection
    }
    cycleDeVie {
      chrono {
        dateCreation
        dateDepot
        datePublication
        datePublicationWeb
      }
    }
    denominationStructurelle
    # divisions { to  unflatten }
    documentsIndexes
    dossierRef${dossier}
    imprimerie {
      prix
    }
    indexation {
      ...IndexationFragment
    }
    legislature
    notice {
      adoptionConforme
      formule
      numNotice
    }
    provenance
    titres {
      titrePrincipal
      titrePrincipalCourt
    }
    uid
  `
    .trim()
    .replace(/^    /gm, "  ".repeat(6))
}

export function generateActeLegislatifFragments({ withDocuments = false, withDossier = false }) {
  const fragmentName =
    `ActeLegislatifFragment${
      withDocuments || withDossier ? "With" : ""
    }${
      withDocuments ? "Documents" : ""
    }${
      withDossier ? "Dossier" : ""
    }`
  let fragments = fragmentsCache[fragmentName]
  if (fragments === undefined) {
    const acteLegislatifShared = `
      codeActe
      dateActe
      libelleActe {
        libelleCourt
        nomCanonique
      }
      organeRef
      organe {
        ...OrganeFragmentWithParent
      }
      uid
    `
      .trim()
      .replace(/^ {6}/gm, "  ".repeat(6))

    const etudePlf = withDocuments
      ? "\n            etudePlf {\n              ...DocumentFragment\n            }"
      : ""
    const pointOdjFragmentName = withDossier ? "PointOdjFragmentWithDossier" : "PointOdjFragment"
    const reunionFragmentName = withDossier ? "ReunionFragmentWithDossier" : "ReunionFragment"
    const texteAdopte = withDocuments
      ? "\n            texteAdopte {\n              ...DocumentFragment\n            }"
      : ""
    const texteAssocie = withDocuments
      ? "\n            texteAssocie {\n              ...DocumentFragment\n            }"
      : ""
    const texteLoi = withDocuments
      ? "\n            texteLoi {\n              ...DocumentFragment\n            }"
      : ""

    fragments = fragmentsCache[fragmentName] = {
      ...generateActeurFragments({ withCommission: true, withGroupe: true, withMandats: false }),
      ...(withDocuments ? generateDocumentFragments({ withDossier }) : {}),
      ...generateInfoJoFragments(),
      ...generateInitiateurFragments(),
      ...generateOrganeFragments({ withParent: true }),
      ...generatePointOdjFragments({ withDossier }),
      ...generateReunionFragments({ withDossier }),
      ...generateScrutinFragments(),
      [fragmentName]: `
        fragment ${fragmentName} on ActeLegislatif {
          ... on AdoptionEurope {
            codeActe
            dateActe
            infoJoce {
              dateJoce
              refJoce
            }
            libelleActe {
              libelleCourt
              nomCanonique
            }
            statutAdoption ${codier}
            texteEuropeen {
              titreTexteEuropeen
              typeTexteEuropeen
            }
            uid
          }
          ... on ConclusionEtapeCc {
            ${acteLegislatifShared}
            anneeDecision
            numDecision
            statutConclusion ${codier}
            urlConclusion
          }
          ... on CreationOrganeTemporaire {
            ${acteLegislatifShared}
            initiateursRef
            initiateurs {
              ...OrganeFragmentWithParent
            }
          }
          ... on Decision {
            ${acteLegislatifShared}
            reunionRef
            reunion {
              ...${reunionFragmentName}
            }
            statutConclusion ${codier}
            textesAssocies {
              refTexteAssocie${texteAssocie}
              typeTexte
            }
            voteRefs
            votes {
              ...ScrutinFragment
            }
          }
          ... on DecisionMotionCensure {
            ${acteLegislatifShared}
            decision ${codier}
            voteRefs
            votes {
              ...ScrutinFragment
            }
          }
          ... on DecisionRecevabiliteBureau {
            ${acteLegislatifShared}
            decision ${codier}
            formuleDecision
          }
          ... on DeclarationGouvernement {
            ${acteLegislatifShared}
            texteAssocieRef${texteAssocie}
            typeDeclaration ${codier}
          }
          ... on DepotAccordInternational {
            ${acteLegislatifShared}
            texteAssocieRef${texteAssocie}
          }
          ... on DepotAvisConseilEtat {
            ${acteLegislatifShared}
            texteAssocieRef${texteAssocie}
          }
          ... on DepotInitiative {
            ${acteLegislatifShared}
            texteAssocieRef${texteAssocie}
          }
          ... on DepotInitiativeNavette {
            ${acteLegislatifShared}
            provenanceRef
            provenance {
              ...OrganeFragmentWithParent
            }
            texteAssocieRef${texteAssocie}
          }
          ... on DepotLettreRectificative {
            ${acteLegislatifShared}
            texteAssocieRef${texteAssocie}
          }
          ... on DepotMotionCensure {
            ${acteLegislatifShared}
            auteursRefs
            auteurs {
              ...ActeurFragmentWithCommissionGroupe
            }
            typeMotionCensure ${codier}
          }
          ... on DepotMotionReferendaire {
            ${acteLegislatifShared}
            auteursRefs
            auteurs {
              ...ActeurFragmentWithCommissionGroupe
            }
          }
          ... on DepotRapport {
            ${acteLegislatifShared}
            texteAdopteRef${texteAdopte}
            texteAssocieRef${texteAssocie}
          }
          ... on DiscussionCommission {
            ${acteLegislatifShared}
            odjRef
            odj {
              ...${pointOdjFragmentName}
            }
            reunionRef
            reunion {
              ...${reunionFragmentName}
            }
          }
          ... on DiscussionSeancePublique {
            ${acteLegislatifShared}
            odjRef
            odj {
              ...${pointOdjFragmentName}
            }
            reunionRef
            reunion {
              ...${reunionFragmentName}
            }
          }
          ... on Etape {
            actesLegislatifsIndexes
            codeActe
            # dateActe
            libelleActe {
              libelleCourt
              nomCanonique
            }
            organeRef
            organe {
              ...OrganeFragmentWithParent
            }
            uid
          }
          ... on EtudeImpact {
            ${acteLegislatifShared}
            contributionInternaute {
              dateFermeture
              dateOuverture
            }
            texteAssocieRef${texteAssocie}
          }
          ... on MotionProcedure {
            ${acteLegislatifShared}
            auteurMotionRef
            auteurMotion {
              ...ActeurFragmentWithCommissionGroupe
            }
            typeMotion ${codier}
          }
          ... on NominRapporteurs {
            ${acteLegislatifShared}
            rapporteurs {
              acteurRef
              acteur {
                ...ActeurFragmentWithCommissionGroupe
              }
              etudePlfRef${etudePlf}
              typeRapporteur
            }
          }
          ... on ProcedureAcceleree {
            ${acteLegislatifShared}
          }
          ... on Promulgation {
            ${acteLegislatifShared}
            codeLoi
            infoJo {
              ...InfoJoFragment
            }
            infoJoRect {
              ...InfoJoFragment
            }
            referenceNor
            texteLoiRef${texteLoi}
            titreLoi
            urlEcheancierLoi
            urlLegifrance
          }
          ... on RenvoiCmp {
            ${acteLegislatifShared}
            initiateur {
              ...InitiateurFragment
            }
          }
          ... on RenvoiPrealable {
            ${acteLegislatifShared}
          }
          ... on RetraitInitiative {
            ${acteLegislatifShared}
            texteAssocieRef${texteAssocie}
          }
          ... on SaisieComAvis {
            ${acteLegislatifShared}
          }
          ... on SaisieComFond {
            ${acteLegislatifShared}
          }
          ... on SaisineConseilConstit {
            ${acteLegislatifShared}
            casSaisine ${codier}
            initiateursRef
            initiateurs {
              ...ActeurFragmentWithCommissionGroupe
            }
            motif
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateActeurFragments({ withCommission = false, withGroupe = false, withMandats = false }) {
  const fragmentName =
    `ActeurFragment${
      withCommission || withGroupe || withMandats ? "With" : ""
    }${
      withCommission ? "Commission" : ""
    }${
      withGroupe ? "Groupe" : ""
    }${
      withMandats ? "Mandats" : ""
    }`
  let fragments = fragmentsCache[fragmentName]
  if (fragments === undefined) {
    const commissionPermanente = withCommission
      ? "commissionPermanente(date: $date) {\n            libelle\n          }\n          "
      : ""
    const groupePolitique = withGroupe
      ? "groupePolitique(date: $date) {\n            libelle\n          }\n          "
      : ""
    const mandats = withMandats
      ? "mandats(date: $date) {\n            ...MandatFragment\n          }\n          "
      : ""
    fragments = fragmentsCache[fragmentName] = {
      ...(withMandats ? generateMandatFragments() : {}),
      [fragmentName]: `
        fragment ${fragmentName} on Acteur {
          ${commissionPermanente}etatCivil {
            ident {
              civ
              nom
              prenom
            }
          }
          ${groupePolitique}${mandats}photo {
            chemin
            cheminMosaique
            hauteur
            largeur
            xMosaique
            yMosaique
          }
          uid
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateAmendementFragments() {
  let fragments = fragmentsCache.AmendementFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.AmendementFragment = {
      ...generateActeurFragments({ withCommission: true, withGroupe: true, withMandats: false }),
      ...generateOrganeFragments({ withParent: true }),
      AmendementFragment: `
        fragment AmendementFragment on Amendement {
          amendementParent
          article99
          cardinaliteAmdMultiples
          corps {
            dispositif
            cartoucheDelaiDepotDepasse
            dispositifAmdtCredit {
              listeProgrammes {
                action
                ae {
                  montantNegatif
                  montantPositif
                }
                cp {
                  montantNegatif
                  montantPositif
                }
                id
                libelle
                lignesCredits {
                  ae {
                    montantNegatif
                    montantPositif
                  }
                  cp {
                    montantNegatif
                    montantPositif
                  }
                  id
                  libelle
                }
              }
              totalAe {
                montantNegatif
                montantPositif
                solde
              }
              totalCp {
                montantNegatif
                montantPositif
                solde
              }
            }
            exposeSommaire
            totalAe {
              montantNegatif
              montantPositif
              solde
            }
            totalCp {
              montantNegatif
              montantPositif
              solde
            }
          }
          dateDepot
          dateDistribution
          etapeTexte
          etat
          identifiant {
            legislature
            numero
            numRect
            saisine {
              mentionSecondeDeliberation
              numeroPartiePlf
              organeExamen
              refTexteLegislatif
            }
          }
          loiReference {
            codeLoi
            divisionCodeLoi
          }
          numeroLong
          pointeurFragmentTexte {
            alinea {
              alineaDesignation
              avantAApres
              numero
            }
            division {
              articleAdditionnel
              articleDesignationCourte
              avantAApres
              chapitreAdditionnel
              divisionRattachee
              titre
              typeTexte
              urlDivisionTexteVise
            }
            missionVisee {
              codeMissionPlf
              idMissionAn
              libelleMission
              libelleMissionPlf
            }
          }
          representations {
            contenu {
              documentUri
            }
            dateDispoRepresentation
            nom
            repSource
            statutRepresentation {
              canonique
              enregistrement
              officielle
              transcription
              verbatim
            }
            typeMime {
              mimeType
              subType
            }
          }
          seanceDiscussion
          signataires {
            auteur {
              acteurRef
              acteur {
                ...ActeurFragmentWithCommissionGroupe
              }
              groupePolitiqueRef
              groupePolitique {
                ...OrganeFragmentWithParent
              }
              organeRef
              organe {
                ...OrganeFragmentWithParent
              }
              typeAuteur
            }
            cosignatairesRefs
            cosignataires {
              ...ActeurFragmentWithCommissionGroupe
            }
            texteAffichable
          }
          sort {
            dateSaisie
            sortEnSeance
          }
          triAmendement
          uid
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateAuteurLegFragments() {
  let fragments = fragmentsCache.AuteurLegFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.AuteurLegFragment = {
      ...generateActeurFragments({ withCommission: true, withGroupe: true, withMandats: false }),
      ...generateMandatFragments(),
      ...generateOrganeFragments({ withParent: true }),
      AuteurLegFragment: `
        fragment AuteurLegFragment on AuteurLeg {
          acteur {
            acteurRef
            acteur {
              ...ActeurFragmentWithCommissionGroupe
            }
            mandatRef
            mandat {
              ...MandatFragment
            }
            qualite
          }
          organeRef
          organe {
            ...OrganeFragmentWithParent
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateClassificationFragments() {
  let fragments = fragmentsCache.ClassificationFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.ClassificationFragment = {
      ClassificationFragment: `
        fragment ClassificationFragment on Classification {
          famille {
            classe {
              code
              libelle
            }
            depot {
              code
              libelle
            }
            espece {
              code
              libelle
            }
          }
          sousType  {
            code
            libelle
            libelleEdition
          }
          statutAdoption
          typeClassification {
            code
            libelle
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateDocumentFragments({ withDossier = false }) {
  const fragmentName = withDossier ? "DocumentFragmentWithDossier" : "DocumentFragment"
  let fragments = fragmentsCache[fragmentName]
  if (fragments === undefined) {
    const texteLoiFragmentName = withDossier ? "TexteLoiFragmentWithDossier" : "TexteLoiFragment"

    fragments = fragmentsCache[fragmentName] = {
      ...generateAuteurLegFragments(),
      ...generateClassificationFragments(),
      ...(withDossier ? generateDossierParlementaireFragments({ withDocuments: false }) : {}),
      ...generateIndexationFragments(),
      ...generateTexteLoiFragments({ withDossier }),
      [fragmentName]: `
        fragment ${fragmentName} on Document {
          ... on AccordInternational {
            ${documentShared(withDossier)}
          }
          ... on AvisConseilEtat {
            ${documentShared(withDossier)}
          }
          ... on DocumentEtudeImpact {
            ${documentShared(withDossier)}
          }
          ... on RapportParlementaire {
            ${documentShared(withDossier)}
          }
          ... on TexteLoi {
            ...${texteLoiFragmentName}
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateDossierParlementaireFragments({ withDocuments = false }) {
  const fragmentName = withDocuments ? "DossierParlementaireFragmentWithDocuments" : "DossierParlementaireFragment"
  let fragments = fragmentsCache[fragmentName]
  if (fragments === undefined) {
    const acteLegislatifFragmentName = withDocuments ? "ActeLegislatifFragmentWithDocuments" : "ActeLegislatifFragment"
    const dossierParlementaireShared = `
      actesLegislatifs {
        actes {
          ...${acteLegislatifFragmentName}
        }
        actesIndexes
      }
      fusionDossier {
        cause
        dossierAbsorbantRef
      }
      indexation {
        ...IndexationFragment
      }
      initiateur {
        ...InitiateurFragment
      }
      legislature
      procedureParlementaire {
        code
        libelle
      }
      titreDossier {
        ...TitreDossierFragment
      }
      uid
    `
      .trim()
      .replace(/^ {6}/gm, "  ".repeat(6))

    const etudePlf = withDocuments
      ? "\n            etudePlf {\n              ...DocumentFragment\n            }"
      : ""

    const texteAssocie = withDocuments
      ? "\n            texteAssocie {\n              ...DocumentFragment\n            }"
      : ""

    fragments = fragmentsCache[fragmentName] = {
      ...generateActeLegislatifFragments({ withDocuments, withDossier: false }),
      ...generateActeurFragments({ withCommission: true, withGroupe: true, withMandats: false }),
      ...generateIndexationFragments(),
      ...generateInitiateurFragments(),
      ...generateOrganeFragments({ withParent: true }),
      ...generateTitreDossierFragments(),
      [fragmentName]: `
        fragment ${fragmentName} on DossierParlementaire {
          ... on DossierCommissionEnquete {
            ${dossierParlementaireShared}
          }
          ... on DossierInitiativeExecutif {
            ${dossierParlementaireShared}
          }
          ... on DossierLegislatif {
            ${dossierParlementaireShared}
            plf {
              missionMinefi {
                libelleCourt
                libelleLong
                missions {
                  libelleCourt
                  libelleLong
                  typeBudget
                  typeMission
                }
                typeBudget
                typeMission
              }
              ordreCommission
              ordreDiqs
              organeRef
              organe {
                ...OrganeFragmentWithParent
              }
              rapporteurs {
                acteurRef
                acteur {
                  ...ActeurFragmentWithCommissionGroupe
                }
                etudePlfRef${etudePlf}
                typeRapporteur
              }
              texteAssocieRef${texteAssocie}
              uid
            }
          }
          ... on DossierMissionControle {
            ${dossierParlementaireShared}
          }
          ... on DossierMissionInformation {
            ${dossierParlementaireShared}
          }
          ... on DossierResolutionAN {
            ${dossierParlementaireShared}
          }
          ... on DossierSansType {
            ${dossierParlementaireShared}
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateIndexationFragments() {
  let fragments = fragmentsCache.IndexationFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.IndexationFragment = {
      IndexationFragment: `
        fragment IndexationFragment on Indexation {
          themes {
            niveau
            theme {
              libelleTheme
            }
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateInfoJoFragments() {
  let fragments = fragmentsCache.InfoJoFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.InfoJoFragment = {
      InfoJoFragment: `
        fragment InfoJoFragment on InfoJo {
          typeJo
          dateJo
          numJo
          urlLegifrance
          referenceNor
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateInitiateurFragments() {
  let fragments = fragmentsCache.InitiateurFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.InitiateurFragment = {
      ...generateActeurFragments({ withCommission: true, withGroupe: true, withMandats: false }),
      ...generateMandatFragments(),
      ...generateOrganeFragments({ withParent: true }),
      InitiateurFragment: `
        fragment InitiateurFragment on Initiateur {
          acteurs {
            acteurRef
            acteur {
              ...ActeurFragmentWithCommissionGroupe
            }
            mandatRef
            mandat {
              ...MandatFragment
            }
          }
          organes {
            ...OrganeFragmentWithParent
          }
          organesRefs
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateMandatFragments() {
  let fragments = fragmentsCache.MandatFragment
  if (fragments === undefined) {
    const mandatShared = `
      infosQualite {
        codeQualite
        libQualite
        libQualiteSex
      }
      nominPrincipale
      typeOrgane
      organes {
        ...OrganeFragmentWithParent
      }
    `
      .trim()
      .replace(/^ {6}/gm, "  ".repeat(6))

    fragments = fragmentsCache.MandatFragment = {
      ...generateOrganeFragments({ withParent: true }),
      MandatFragment: `
        fragment MandatFragment on Mandat {
          ... on MandatAvecSuppleant {
            ${mandatShared}
          }
          ... on MandatMission{
            ${mandatShared}
          }
          ... on MandatParlementaire{
            ${mandatShared}
          }
          ... on MandatSimple {
            ${mandatShared}
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateOrganeFragments({ withParent = false }) {
  const fragmentName = withParent ? "OrganeFragmentWithParent" : "OrganeFragment"
  let fragments = fragmentsCache[fragmentName]
  if (fragments === undefined) {
    const organeParent = withParent
      ? "\n      organeParent {\n        ...OrganeFragment\n      }"
      : ""

    const organeShared = `
      codeType
      libelle
      libelleAbrege
      libelleAbrev
      libelleEdition
      organeParentRef${organeParent}
      uid
      vimode {
        dateAgrement
        dateDebut
        dateFin
      }
    `
      .trim()
      .replace(/^ {6}/gm, "  ".repeat(6))

    fragments = fragmentsCache[fragmentName] = {
      ...(withParent ? generateOrganeFragments({ withParent: false }) : {}),
      [fragmentName]: `
        fragment ${fragmentName} on Organe {
          ... on GroupePolitique {
            ${organeShared}
            legislature
            positionPolitique
            regime
            secretariat {
              secretaire01
              secretaire02
            }
          }
          ... on OrganeExterne {
            ${organeShared}
          }
          ... on  OrganeExtraParlementaire{
            ${organeShared}
            nombreReunionsAnnuelles
            regime
            regimeJuridique
            siteInternet
          }
          ... on  OrganeParlementaire {
            ${organeShared}
            legislature
            regime
            secretariat {
              secretaire01
              secretaire02
            }
          }
          ... on  OrganeParlementaireInternational{
            ${organeShared}
            legislature
            paysRefs
            regime
            secretariat {
              secretaire01
              secretaire02
            }
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generatePointOdjFragments({ withDossier = false }) {
  const fragmentName =
    `PointOdjFragment${
      withDossier ? "With" : ""
    }${
      withDossier ? "Dossier" : ""
    }`
  let fragments = fragmentsCache[fragmentName]
  if (fragments === undefined) {
    const dossiersLegislatifs = withDossier
      ? "\n                dossiersLegislatifs {\n                  ...DossierParlementaireFragment\n                }"
      : ""

    fragments = fragmentsCache[fragmentName] = {
      ...(withDossier ? generateDossierParlementaireFragments({ withDocuments: false }) : {}),
      [fragmentName]: `
        fragment ${fragmentName} on PointOdj {
          ... on PodjReunion {
            comiteSecret
            cycleDeVie {
              chrono {
                cloture
                creation
              }
              etat
            }
            dossiersLegislatifsRefs${dossiersLegislatifs}
            objet
            procedure
            typePointOdj
            uid
          }
          ... on PodjSeanceConfPres {
            comiteSecret
            cycleDeVie {
              chrono {
                cloture
                creation
              }
              etat
            }
            dateConfPres
            dateLettreMinistre
            dossiersLegislatifsRefs${dossiersLegislatifs}
            natureTravauxOdj
            objet
            procedure
            typePointOdj
            uid
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateReunionFragments({ withDossier = false }) {
  const fragmentName =
    `ReunionFragment${
      withDossier ? "With" : ""
    }${
      withDossier ? "Dossier" : ""
    }`
  let fragments = fragmentsCache[fragmentName]
  if (fragments === undefined) {
    const pointOdjFragmentName = withDossier ? "PointOdjFragmentWithDossier" : "PointOdjFragment"

    fragments = fragmentsCache[fragmentName] = {
      ...generateActeurFragments({ withCommission: false, withGroupe: false, withMandats: false }),
      ...generateOrganeFragments({ withParent: true }),
      ...generatePointOdjFragments({ withDossier }),
      [fragmentName]: `
        fragment ${fragmentName} on Reunion {
          compteRenduRef
          cycleDeVie {
            chrono {
              cloture
              creation
            }
            etat
          }
          demandeurs {
            acteurs {
              ...ActeurFragment
            }
            acteursRefs
            organeRef
          }
          formatReunion
          identifiants {
            dateSeance
            idJo
            numSeanceJo
            quantieme
          }
          infosReunionsInternationale {
            estReunionInternationale
            paysRefs
          }
          lieu {
            code
            libelleCourt
            libelleLong
          }
          odj {
            convocationOdj
            pointsOdj {
              ...${pointOdjFragmentName}
            }
            resumeOdj
          }
          organeReuni {
            ...OrganeFragmentWithParent
          }
          organeReuniRef
          ouverturePresse
          sessionRef
          timestampDebut
          timestampFin
          typeReunion
          uid
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateScrutinFragments() {
  let fragments = fragmentsCache.ScrutinFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.ScrutinFragment = {
      ScrutinFragment: `
        fragment ScrutinFragment on Scrutin {
          dateScrutin
          # TODO
          titre
          uid
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateTexteLegFragments() {
  let fragments = fragmentsCache.TexteLegFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.TexteLegFragment = {
      ...generateAmendementFragments(),
      ...generateDocumentFragments({ withDossier: true }),
      TexteLegFragment: `
        fragment TexteLegFragment on TexteLeg {
          amendements {
            acteurRef
            acteur {
              ...ActeurFragmentWithCommissionGroupe
            }
            amendements {
              ...AmendementFragment
            }
            article99
            articleAdditionnel
            canonique
            # codeLoi
            dateDepot
            dateDispoRepresentation
            dateDistribution
            documentUri
            libelle
            loiReference {
              codeLoi
              divisionCodeLoi
            }
            montantNegatif
            montantPositif
            nom
            officielle
            repSource
            seanceDiscussion
            sort {
              dateSaisie
              sortEnSeance
            }
            subType
            text
            transcription
            typeMime {
              mimeType
              subType
            }
            verbatim
          }
          refTexteLegislatif
          texteLegislatif {
            ...DocumentFragmentWithDossier
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateTexteLoiFragments({ withDossier = false }) {
  const fragmentName = withDossier ? "TexteLoiFragmentWithDossier" : "TexteLoiFragment"
  let fragments = fragmentsCache[fragmentName]
  if (fragments === undefined) {
    fragments = fragmentsCache[fragmentName] = {
      ...generateActeurFragments({ withCommission: true, withGroupe: true, withMandats: false }),
      ...(withDossier ? generateDossierParlementaireFragments({ withDocuments: false }) : {}),
      ...generateOrganeFragments({ withParent: true }),
      [fragmentName]: `
        fragment ${fragmentName} on TexteLoi {
          ${documentShared(withDossier)}
          cosignataires {
            acteurRef
            acteur {
              ...ActeurFragmentWithCommissionGroupe
            }
            dateCosignature
            dateRetraitCosignature
            edite
            organe {
              etApparentes
              organeRef
              organe {
                ...OrganeFragmentWithParent
              }
            }
          }
          depotAmendements {
            amendementsCommission {
              amendable
              organeRef
              organe {
                ...OrganeFragmentWithParent
              }
            }
            amendementsSeance {
              amendable
            }
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}

export function generateTitreDossierFragments() {
  let fragments = fragmentsCache.TitreDossierFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.TitreDossierFragment = {
      TitreDossierFragment: `
        fragment TitreDossierFragment on TitreDossier {
          titre
          titreChemin
          senatChemin
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}
