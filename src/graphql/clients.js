import { InMemoryCache, IntrospectionFragmentMatcher } from "apollo-cache-inmemory"
import { ApolloClient } from "apollo-client"
// import { ApolloLink } from "apollo-link"
import { HttpLink } from "apollo-link-http"
// import { WebSocketLink } from "apollo-link-ws"
// import { getMainDefinition } from "apollo-utilities"
import fetch from "cross-fetch"
// import gql from "graphql-tag"
// import { SubscriptionClient } from "subscriptions-transport-ws"
// import WebSocket from "universal-websocket-client"
import resolveUrl from "url-resolve"

import config from "../config"
// Cf https://www.apollographql.com/docs/react/recipes/fragment-matching.html
// import apiFragmentTypes from "./fragment_types/api.json"
import assembleeApiFragmentTypes from "./fragment_types/assemblee_api.json"
import senatApiFragmentTypes from "./fragment_types/senat_api.json"

// const ameliAmdFragment = gql`
//   fragment AmeliAmdFragment on AmeliAmd {
//     id
//     sub {
//       ...AmeliSubFragment
//     }
//     subid
//     amdperid
//     motid
//     etaid
//     noment {
//       ...AmeliEntFragment
//     }
//     nomentid
//     sor {
//       ...AmeliSorFragment
//     }
//     sorid
//     avc {
//       ...AmeliAviFragment
//     }
//     avcid
//     avg {
//       ...AmeliAviFragment
//     }
//     avgid
//     irrid
//     txtid
//     opmid
//     ocmid
//     ideid
//     discomid
//     num
//     rev
//     typ
//     dis
//     obj
//     datdep
//     obs
//     ord
//     autext
//     subpos
//     mot
//     numabs
//     subidder
//     libgrp
//     alinea
//     accgou
//     colleg
//     typrectid
//     islu
//     motposexa
//   }
// `

// const ameliAviFragment = gql`
//   fragment AmeliAviFragment on AmeliAvi {
//     id
//     lib
//     cod
//   }
// `

// const ameliGrpPolFragment = gql`
//   fragment AmeliGrpPolFragment on AmeliGrpPol {
//     id
//     typ
//     act
//     entid
//     cod
//     libcou
//     lilcou
//     codint
//     tri
//   }
// `

// const ameliEntFragment = gql`
//   fragment AmeliEntFragment on AmeliEnt {
//     ... on AmeliCab {
//       id
//       typ
//       act
//       entid
//       codint
//       lilOptional: lil
//     }
//     ... on AmeliCom {
//       id
//       typ
//       act
//       entid
//       cod
//       lib
//       lil
//       spc
//       codint
//       tri
//     }
//     ... on AmeliGrpPol {
//       ...AmeliGrpPolFragment
//     }
//     ... on AmeliGvt {
//       id
//       typ
//       act
//       entid
//       nom
//       pre
//       qua
//       tit
//     }
//     ... on AmeliSen {
//       id
//       typ
//       act
//       entid
//       grp {
//         ...AmeliGrpPolFragment
//       }
//       grpid
//       comid
//       comspcid
//       mat
//       qua
//       nomuse
//       prenomuse
//       nomtec
//       hom
//       app
//       ratt
//       nomusemin
//       senfem
//     }
//   }
// `

// const ameliNatFragment = gql`
//   fragment AmeliNatFragment on AmeliNat {
//     id
//     lib
//   }
// `

// const ameliSesFragment = gql`
//   fragment AmeliSesFragment on AmeliSes {
//     id
//     typid
//     ann
//     lil
//   }
// `

// const ameliSubFragment = gql`
//   fragment AmeliSubFragment on AmeliSub {
//     id
//     txtid
//     merid
//     typ {
//       ...AmeliTypSubFragment
//     }
//     typid
//     lic
//     lib
//     pos
//     sig
//     posder
//     prires
//     dupl
//     subamd
//     sorid
//     txtidder
//     style
//     islec
//   }
// `

// const ameliSorFragment = gql`
//   fragment AmeliSorFragment on AmeliSor {
//     id
//     lib
//     cod
//     typ
//   }
// `

// const ameliTypSubFragment = gql`
//   fragment AmeliTypSubFragment on AmeliTypSub {
//     id
//     lib
//   }
// `

// const ameliTxtFragment = gql`
//   fragment AmeliTxtFragment on AmeliTxt {
//     id
//     nat {
//       ...AmeliNatFragment
//     }
//     natid
//     lecid
//     sesins {
//       ...AmeliSesFragment
//     }
//     sesinsid
//     sesdep {
//       ...AmeliSesFragment
//     }
//     sesdepid
//     fbuid
//     num
//     int
//     inl
//     datdep
//     urg
//     dis
//     secdel
//     loifin
//     loifinpar
//     txtamd
//     datado
//     numado
//     txtexa
//     pubdellim
//     numabs
//     libdelim
//     libcplnat
//     doslegsignet
//     proacc
//     txttyp
//     ordsnddelib
//     txtetaid
//     fusderid
//     fusder
//     fusderord
//     fusdertyp
//   }
// `

// const anDossierFragment = gql`
//   fragment AnDossierFragment on AnDossier {
//     id
//     titre
//     titre_chemin
//     senat_chemin
//     procedure_code
//     procedure_libelle
//     legislature_id
//   }
// `

// const amendementFragment = gql`
//   fragment AmendementFragment on Amendement {
//     auteur {
//       ...AuteurFragment
//     }
//     bibard
//     bibardSuffixe
//     cosignataires {
//       ...AuteurFragment
//     }
//     cosignatairesMentionLibre {
//       ...CosignatairesMentionLibreFragment
//     }
//     dispositif
//     etat
//     exposeSommaire
//     legislature
//     numero
//     numeroLong
//     numeroReference
//     organeAbrv
//     place
//     placeReference
//     sortEnSeance
//   }
// `

// const auteurFragment = gql`
//   fragment AuteurFragment on Auteur {
//     civilite
//     estRapporteur
//     estGouvernement
//     groupeTribunId
//     nom
//     photoUrl
//     prenom
//     qualite
//     tribunId
//   }
// `

// const cosignatairesMentionLibreFragment = gql`
//   fragment CosignatairesMentionLibreFragment on CosignatairesMentionLibre {
//     titre
//   }
// `

// const discussionFragment = gql`
//   fragment DiscussionFragment on Discussion {
//     amendements {
//       ...EnveloppeAmendementFragment
//     }
//     bibard
//     bibardSuffixe
//     divisions {
//       ...DivisionFragment
//     }
//     legislature
//     numeroProchainADiscuter
//     organe
//     pages {
//       ...PageFragment
//     }
//     titre
//     type
//   }
// `

// const divisionFragment = gql`
//   fragment DivisionFragment on Division {
//     # amendements
//     place
//     position
//   }
// `

// const elementOrdreDuJourFragment = gql`
//   fragment ElementOrdreDuJourFragment on ElementOrdreDuJour {
//     textBibard
//     textBibardSuffixe
//     textTitre
//   }
// `

// const enveloppeAmendementFragment = gql`
//   fragment EnveloppeAmendementFragment on EnveloppeAmendement {
//     alineaLabel
//     amendement {
//       ...AmendementFragment
//     }
//     auteurGroupe
//     auteurLabel
//     discussionCommune
//     discussionCommuneAmdtPositon
//     discussionCommuneSsAmdtPositon
//     discussionIdentique
//     discussionIdentiqueAmdtPositon
//     discussionIdentiqueSsAmdtPositon
//     missionLabel
//     numero
//     parentNumero
//     place
//     position
//     sort
//   }
// `

// const pageFragment = gql`
//   fragment PageFragment on Page {
//     body
//     numero
//     slug
//     titre
//   }
// `

// const prochainADiscuterFragment = gql`
//   fragment ProchainADiscuterFragment on ProchainADiscuter {
//     bibard
//     bibardSuffixe
//     legislature
//     nbrAmdtRestant
//     numAmdt
//     organeAbrv
//   }
// `

// const referenceOrganeFragment = gql`
//   fragment ReferenceOrganeFragment on ReferenceOrgane {
//     text
//     value
//   }
// `

// const amendementUpsertedSubscriptionQuery = gql`
//   subscription onAmendementUpserted {
//     amendementUpserted {
//       ...AmendementFragment
//     }
//   }
//   ${amendementFragment}
//   ${auteurFragment}
//   ${cosignatairesMentionLibreFragment}
// `

// export function createAmendementUpsertedSubscriptionQuery() {
//   return {
//     query: amendementUpsertedSubscriptionQuery,
//     variables: {},
//   }
// }

// const assembleeDiscussionQuery =
//   gql`
//     query Discussion($bibard: String!, $bibardSuffixe: String, $legislature: Int!, $organe: String!) {
//       discussion(bibard: $bibard, bibardSuffixe: $bibardSuffixe, legislature: $legislature, organe: $organe) {
//         ...DiscussionFragment
//       }
//     }
//   ` +
//   amendementFragment +
//   auteurFragment +
//   cosignatairesMentionLibreFragment +
//   discussionFragment +
//   divisionFragment +
//   enveloppeAmendementFragment +
//   pageFragment

// export function createAssembleeDiscussionQuery(bibard, bibardSuffixe, legislature, organe) {
//   legislature = parseInt(legislature)
//   return {
//     query: assembleeDiscussionQuery,
//     variables: {
//       bibard: bibard,
//       bibardSuffixe: bibardSuffixe,
//       legislature: legislature,
//       organe: organe,
//     },
//   }
// }

// const assembleeDossiersQuery = gql`
//   query AssembleeDossiers($legislature: Int!) {
//     assembleeDossiers(legislature: $legislature) {
//       ...AnDossierFragment
//     }
//   }
//   ${anDossierFragment}
// `

// export function createAssembleeDossiersQuery(legislature) {
//   return {
//     query: assembleeDossiersQuery,
//     variables: {
//       legislature,
//     },
//   }
// }

// const ordreDuJourQuery =
//   gql`
//     query OrdreDuJour($legislature: Int!, $organe: String!) {
//       ordreDuJour(legislature: $legislature, organe: $organe) {
//         ...ElementOrdreDuJourFragment
//       }
//     }
//   ` + elementOrdreDuJourFragment

// export function createOrdreDuJourQuery(legislature, organe) {
//   legislature = parseInt(legislature)
//   return {
//     query: ordreDuJourQuery,
//     variables: {
//       legislature: legislature,
//       organe: organe,
//     },
//   }
// }

// const referencesOrganesQuery =
//   gql`
//     query ReferencesOrganes($legislature: Int!) {
//       referencesOrganes(legislature: $legislature) {
//         ...ReferenceOrganeFragment
//       }
//     }
//   ` + referenceOrganeFragment

// export function createReferencesOrganesQuery(legislature) {
//   legislature = parseInt(legislature)
//   return {
//     query: referencesOrganesQuery,
//     variables: {
//       legislature: legislature,
//     },
//   }
// }

// const prochainADiscuterUpsertedSubscriptionQuery =
//   gql`
//     subscription onProchainADiscuterUpserted {
//       prochainADiscuterUpserted {
//         ...ProchainADiscuterFragment
//       }
//     }
//   ` + prochainADiscuterFragment

// export function createProchainADiscuterUpsertedSubscriptionQuery() {
//   return {
//     query: prochainADiscuterUpsertedSubscriptionQuery,
//     variables: {},
//   }
// }

// export function createGraphqlClient(httpUrl, wsUrl) {
//   const webSocketClient = new SubscriptionClient(
//     wsUrl,
//     {
//       reconnect: true, //auto-reconnect
//       // // carry login state (should use secure websockets (wss) when using this)
//       // connectionParams: {
//       //   authToken: localStorage.getItem("Meteor.loginToken")
//       // }
//     },
//     WebSocket
//   )
//   const link = ApolloLink.split(
//     function(operation) {
//       // Split link based on operation type.
//       const definition = getMainDefinition(operation.query)
//       return definition.kind === "OperationDefinition" && definition.operation === "subscription"
//     },
//     new WebSocketLink(webSocketClient),
//     new HttpLink({
//       fetch,
//       uri: httpUrl,
//     })
//   )
//   // Finally, create your ApolloClient instance with the modified network interface
//   const fragmentMatcher = new IntrospectionFragmentMatcher({
//     introspectionQueryResultData: apiFragmentTypes,
//   })
//   // See:
//   // * https://www.apollographql.com/docs/react/api/apollo-client.html
//   // * https://www.apollographql.com/docs/react/features/error-handling.html
//   // * https://www.apollographql.com/docs/react/advanced/caching.html#ignore
//   return new ApolloClient({
//     cache: new InMemoryCache({ fragmentMatcher }),
//     link,
//   })
// }

export function createGraphqlClientForAssemblee(defaultOptions) {
  const httpUrl = resolveUrl(config.assembleeApiUrl, "/graphql")
  // const wsUrl = resolveUrl(config.assembleeApiUrl, "/subscriptions").replace(/http/, "ws")
  const link = new HttpLink({
    fetch,
    uri: httpUrl,
  })
  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: assembleeApiFragmentTypes,
  })
  // See:
  // * https://www.apollographql.com/docs/react/api/apollo-client.html
  // * https://www.apollographql.com/docs/react/features/error-handling.html
  // * https://www.apollographql.com/docs/react/advanced/caching.html#ignore
  return new ApolloClient({
    cache: new InMemoryCache({ fragmentMatcher }),
    defaultOptions,
    link,
  })
}

export function createGraphqlClientForSenat(defaultOptions) {
  const httpUrl = resolveUrl(config.senatApiUrl, "/graphql")
  // const wsUrl = resolveUrl(config.senatApiUrl, "/subscriptions").replace(/http/, "ws")
  const link = new HttpLink({
    fetch,
    uri: httpUrl,
  })
  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: senatApiFragmentTypes,
  })
  // See:
  // * https://www.apollographql.com/docs/react/api/apollo-client.html
  // * https://www.apollographql.com/docs/react/features/error-handling.html
  // * https://www.apollographql.com/docs/react/advanced/caching.html#ignore
  return new ApolloClient({
    cache: new InMemoryCache({ fragmentMatcher }),
    defaultOptions,
    link,
  })
}

// const senatDiscussionQuery =
//   gql`
//     query SenatDiscussion($num: String!, $sesinsLil: String!, $txttyp: String!) {
//       senatDiscussion(num: $num, sesinsLil: $sesinsLil, txttyp: $txttyp) {
//         ...AmeliTxtFragment
//         amds {
//           ...AmeliAmdFragment
//         }
//         subs {
//           ...AmeliSubFragment
//         }
//       }
//     }
//   ` +
//   ameliAmdFragment +
//   ameliAviFragment +
//   ameliEntFragment +
//   ameliGrpPolFragment +
//   ameliNatFragment +
//   ameliSesFragment +
//   ameliSorFragment +
//   ameliSubFragment +
//   ameliTxtFragment +
//   ameliTypSubFragment

// export function createSenatDiscussionQuery(num, sesinsLil, txttyp) {
//   return {
//     query: senatDiscussionQuery,
//     variables: {
//       num,
//       sesinsLil,
//       txttyp,
//     },
//   }
// }

export function deleteGraphqlClient(/* graphqlClient */) {
  // TODO: Is there something to do?
}

export function textFromFragments(fragments) {
  return Object.keys(fragments)
    .sort()
    .map(fragmentName => {
      return `\n  ${fragments[fragmentName]}\n`
    })
    .join("\n")
}

export const assembleeGraphqlClient = createGraphqlClientForAssemblee()
export const senatGraphqlClient = createGraphqlClientForSenat()
