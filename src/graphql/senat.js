const fragmentsCache = {}

export function generateLoiFragments() {
  let fragments = fragmentsCache.LoiFragment
  if (fragments === undefined) {
    fragments = fragmentsCache.LoiFragment = {
      LoiFragment: `
        fragment LoiFragment on Loi {
          loicod
          typloi {
            typloicod
            typloilib
            groupe
            typloiden
            typloigen
            typloitit
            typloidenplu
            typloide
            typloiabr
          }
          etaloi {
            etaloicod
            etaloilib
          }
          deccoccod
          numero
          signet
          loient
          motclef
          loitit
          loiint
          urgence
          urlJo
          loinumjo
          loidatjo
          dateLoi
          loititjo
          urlJo2
          loinumjo2
          loidatjo2
          deccocurl
          numDecision
          dateDecision
          loicodmai
          loinoudelibcod
          motionloiorigcod
          objet
          urlOrdonnance
          saisineDate
          saisinePar
          loidatjo3
          loinumjo3
          urlJo3
          urlAn
          urlPresart
          signetalt
          orgcod
          doscocurl
          loiintori
          proaccdat
          proaccoppdat
          retproaccdat
          lectures {
            lecidt
            loicod
            typlec {
              typleccod
              typleclib
              typlecord
            }
            leccom
            lecass {
              lecassidt
              lecidt
              ass {
                codass
                libass
              }
              ordreass
              sesann
              ptlnum
              ptlurl
              ptlnumcpl
              ptlnot
              ptlurl2
              ptlnot2
              ptlurl3
              ptlnot3
              ptlnumcpl2
              ptlnumcpl3
              lecassame
              lecassameses
              orgcod
              loiintmod
              reucom
              debatsurl
              depotOnly
              lecassamedat
              lecassamecomdat
              orippr
              libppr
              sesppr
              ptlurlcom
              aliasppr
              lecassamecom
              lecassamesescom
              ptlnumcom
              auds {
                audcle
                lecassidt
                auddat
                audtit
                audurl
                orgcod
              }
              datesSeances {
                lecidt
                dateS
                code
                statut
              }
            }
          }
        }
      `
        .trim()
        .replace(/^ {6}/gm, ""),
    }
  }
  return fragments
}