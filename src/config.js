import { validateConfig } from "./validators/config"

const config = {
  amendaUrl: "http://localhost:3001/",
  // apiUrl: "http://localhost:2048/",
  assembleeApiUrl: "http://localhost:8000/",
  cache: null,
  enableAuthentication: true,
  globalAlert: {
    class: "is-danger",
    messageHtml: `
      <article class="container mx-auto my-4" role="alert">
        <div class="bg-orange font-bold rounded-t px-4 py-2 text-white">
          <i class="fas fa-exclamation-triangle"></i> Attention !
        </div>
        <div class="bg-orange-lightest border border-orange-light border-t-0 px-4 py-3 rounded-b text-orange-dark">
          <p>
            Ce site est <b>en développement</b>. Les informations qui y figurent peuvent être fausses et surtout
            <b>incomplètes</b>. En cas de doute, référez-vous aux sources des différentes données.
          </p>
          <p class="leading-normal">
            N'hésitez pas à nous
            <a href="https://framagit.org/tricoteuses/tricoteuses-ui/issues" target="_blank" title="Support technique des Tricoteuses">signaler des problèmes</a>
            où même à
            <a href="https://framagit.org/tricoteuses" target="_blank" title="Forge logicielle des Tricoteuses">
              tricoter des améliorations
            </a>.
          </p>
        </div>
      </article>
    `,
  },
  leftMenu: [
    {
      contentHtml: "Travaux parlementaires",
      prefetch: true,
      title: "Projets et propositions de lois, rapports, résolutions, etc",
      url: ".",
    },
    {
      contentHtml: "Travaux citoyens",
      title: "Propositions de lois, amendements",
      url: "http://localhost:3001/",
    },
    {
      contentHtml: "Parlementaires",
      prefetch: true,
      title: "Députés et sénateurs",
      url: "deputes",
    },
    // {
    //   contentHtml: "Lobbyistes",
    //   prefetch: true,
    //   url: "lobbyistes",
    // },
  ],
  missionStatement: "Connaître, comprendre et améliorer le travail parlementaire.",
  rightMenu: [
    {
      contentHtml: "À propos",
      prefetch: true,
      title: "Informations sur ce site web",
      url: "a-propos",
    },
  ],
  searchUrl: "http://localhost:8005/",
  senatApiUrl: "http://localhost:8004/",
  title: "Tricoteuses",
  url: ".",
}

const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(validConfig, null, 2)}\nError:\n${JSON.stringify(error, null, 2)}`
  )
  process.exit(-1)
}

export default validConfig
